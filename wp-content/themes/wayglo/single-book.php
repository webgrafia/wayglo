<?php
/**
 * The main template file
 */

get_header();

if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
		?>
        <main role="main" class="article-wrapper bg-white">
            <div class="container">
                <div class="row sticky-sidebar-container">
                    <div class="col-lg-4">
                        <aside role="complementary" class="article-aside sticky-sidebar">
                            <div class="box box-adv mb-2">
                                <?php
                                $img = get_the_post_thumbnail(get_the_ID(),"book");
                                $featured_full = get_the_post_thumbnail_url(get_the_ID(),'full');
                                if($img){
	                                $featured = '<a href="'.$featured_full.'">'.$img.'</a>';
	                                if ( function_exists( 'slb_activate' ) ) {
		                                $featured = slb_activate( $featured );
	                                }
	                                echo $featured;
                                }
                                ?>
                            </div><!-- /box-adv -->
                            <?php
                            if(get_field("url_shop")){
                            ?>
                            <p class="acquista"><span class="dot-inside"></span> &nbsp; <a class="font-weight-bold" style="text-decoration: none;" href="<?php the_field("url_shop"); ?>" target="_blank">ACQUISTA</a></p>
                            <?php
                            }

                            if(is_multisite()){
	                            global $blog_id;
	                            $current_blog_details = get_blog_details( array( 'blog_id' => $blog_id ) );
	                            if($current_blog_details->blogname == "Wayglo Basilicata"){
                                    ?>
                                    <hr>
                                    <div class="smaller-text mb-1 freetext">
                                        <p>Vuoi segnalare un libro sulla Basilicata? <br>
                                            Scrivici a <b><a href="mailto:basilicata@wayglo.it">basilicata@wayglo.it</a></b></p>
                                    </div>
                                    <hr>
                                    <?php
	                            } else if($current_blog_details->blogname == "Wayglo Roma"){
		                            ?>
                                    <hr>
                                    <div class="smaller-text mb-1 freetext">
                                        <p>Vuoi segnalare un libro su Roma? <br>
                                            Scrivici a <b><a href="mailto:roma@wayglo.it">roma@wayglo.it</a></b></p>
                                    </div>
                                    <hr>
		                            <?php
	                            }else{
                                    echo "<!-- ".$current_blog_details->blogname." //-->";
	                            }
                            }
                            ?>

                        </aside>
                    </div><!-- /col-lg-4 -->
                    <div class="col-lg-6">
                        <div class="breadcrumbs">
	                        <?php wayglo_cat( "btn btn-white text-uppercase" ); ?>
                        </div><!-- /breadcrumb -->
                        <div class="article-title">
                            <h1><?php the_title() ?></h1>
                            <p class="article-subtitle"></p>
                        </div><!-- /article-title -->
                        <article class="article-container">
                            <?php the_content(); ?>
                            <div class="article-footer">
	                            <?php get_template_part("template-parts/common/share-button", "article"); ?>
                            </div><!-- /article-footer -->
                            <?php
                            $link_esterni = get_field("link_esterni", $post);
                            if($link_esterni){
                                echo "<div class='external'>";
	                            foreach ( $link_esterni as $item ) { ?>
                                <p><a target="_blank" href="<?php echo $item["link"]; ?>"><?php echo $item["testo"]; ?><img src="<?php echo $item["immagine"]; ?>" /></a></p>
                                <?php
                                }
	                            echo "</div>";
                            }

                            ?>

	                        <?php get_template_part("template-parts/single/licenza"); ?>

	                        <?php echo do_shortcode(get_field("shortcode_after_content", "option")); ?>

                            <div class="article-comments">
                                <!-- /DISQUS -->
                            </div><!-- /article-comments -->
                        </article>
                    </div><!-- /col-lg-6 -->
                    <div class="col-lg-2">
                        <aside role="complementary" class="article-aside sticky-sidebar">
                            <?php
                            $attributi = get_field("attributi");
                            foreach ( $attributi as $attributo ) { ?>
                                <div class="smaller-text mb-1">
                                    <span class="d-block"><?php echo $attributo["nome"]; ?>:</span>
                                    <strong><?php echo $attributo["valore"]; ?></strong>
                                </div><!-- /smaller-text -->
                            <?php }
                            ?>
                            <div class="smaller-text mb-1 freetext">
                            <?php
                            echo get_field("testo_libero");
                            ?>
                            </div>

	                        <?php get_template_part("template-parts/common/share-button", "aside"); ?>
                            <hr/>
                            <div class="text-tiny-icon mb-1">
                                <svg class="svg-calendar"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-calendar"></use></svg>
                                <p><?php the_date(); ?></p>
                            </div><!-- /text-tiny-icon -->
                            <div class="tags pr-2">
	                            <?php wayglo_tags($post); ?>
                            </div><!-- /tags -->
                        </aside>
                    </div><!-- /col-lg-2 -->
                </div><!-- /row -->
            </div><!-- /container -->
        </main>


		<?php
	}
}
?>
<?php
get_template_part("template-parts/single/related", "book");
?>

<?php
get_footer();
