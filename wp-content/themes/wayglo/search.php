<?php
/**
 * The main template file
 */

get_header();

if ( have_posts() ) {

	$term = get_queried_object();

	$tipologia_apertura = get_field('tipologia_apertura', $term);
	$apertura_manuale = get_field('apertura_manuale', $term);
	$numero_slide = get_field('numero_slide', $term);

	$template = get_field('template', $term);
	$blocchi = get_field('blocchi', $term);

	$origposts = $posts;

	$post = $posts[0];
	$skip = 1;
	if($apertura_manuale && count($apertura_manuale)){
		$post = $apertura_manuale[0];
		$skip = 0;
	}
	get_template_part("template-parts/single/hero");



	$posts = $origposts;
	wp_reset_postdata();


	$c=1;$r=1;$banner=1;
	while ( have_posts() ) {
		the_post();
		if($c > $skip){
			// apro la sezione
			if(($r == 1) || ($r == 5)) {
				?>
                <section class="section mb-3">
                <div class="container">
                <div class="row sticky-sidebar-container">
                <div class="col-lg-8">
				<?php
			}
			get_template_part("template-parts/single/box-article-horizontal");
			// chiudo la sezione
			if(($r == 4) || ($r == (count($posts) - $skip))) { ?>
                </div><!-- /col-lg-8 -->
                <div class="col-lg-4">
                    <aside role="complementary" class="section-aside sticky-sidebar">
						<?php
						get_template_part("template-parts/banner/aside", $banner);
						?>
                    </aside>
                </div><!-- /col-lg-4 -->
                </div><!-- /row -->
                </div><!-- /container -->
                </section><!-- /section -->

                <section class="section section-stage mb-3">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
								<?php
								get_template_part("template-parts/banner/archive", $banner);
								?>
                            </div><!-- /col-12 -->
                        </div><!-- /row --

                            </div><!-- /container -->
                </section><!-- /section -->
				<?php
			}
			$r ++;
		}
		$c++;
	}
	get_template_part( "template-parts/common/pager" );

}else{

	get_template_part("template-parts/common/404");

}
?>
<?php
get_footer();
