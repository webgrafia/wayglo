<?php
/**
 * The main template file
 */

get_header();

if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
		?>
        <main role="main" class="article-wrapper bg-white">
            <div class="container">
                <div class="row sticky-sidebar-container">
                    <div class="col-lg-2">
                        <aside role="complementary" class="article-aside sticky-sidebar">
							<?php get_template_part("template-parts/common/share-button", "aside"); ?>
                            <hr/>
                            <div class="text-tiny-icon mb-1">
                                <svg class="svg-calendar"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-calendar"></use></svg>
                                <p><?php the_date(); ?></p>
                            </div><!-- /text-tiny-icon -->
                            <div class="tags pr-2">
								<?php wayglo_tags($post); ?>
                            </div><!-- /tags -->

							<?php
							$fonte = get_field("fonte_notizia");
							if($fonte){
								?>
                                <hr/>
                                <div class="smaller-text">
                                    <span class="d-block">fonte:</span>
                                    <strong><?php echo $fonte; ?></strong>
                                </div><!-- /smaller-text -->
								<?php
							}
							?>
                        </aside>
                    </div><!-- /col-lg-2 -->
                    <div class="col-lg-6">
                        <div class="breadcrumbs">
	                        <?php wayglo_cat( "btn btn-white text-uppercase" ); ?>
                        </div><!-- /breadcrumb -->
                        <div class="article-title">
                            <h1><?php the_title(); ?></h1>
                            <p class="article-subtitle"></p>
                        </div><!-- /article-title -->
                        <article class="article-container">
                            <?php
		                 //   if(!get_field("apertura_video")){
                            ?>

                            <?php
                   //         }
                            if(get_field("apertura_video")){
                             $urlvideo = get_field("url_video");
                             if($urlvideo) {
	                             echo "<div class='videoWrapper'>";
	                             echo wp_oembed_get( $urlvideo );
	                             echo "</div>";
                             }
                            }else{
							if(has_post_thumbnail()){
								?>
                            <div class="featured">
	                            <?php
	                            $img = get_the_post_thumbnail(get_the_ID(),"apertura");
	                            $featured_full = get_the_post_thumbnail_url(get_the_ID(),'full');
	                            if($img){
		                            $featured = '<a href="'.$featured_full.'">'.$img.'</a>';
		                            if ( function_exists( 'slb_activate' ) ) {
			                            $featured = slb_activate( $featured );
		                            }
		                            echo $featured;
	                            }
	                            ?>

                                <?php
								echo "<p>";
								the_post_thumbnail_caption(get_the_ID());
								echo "</p>";

								?>
                            </div>

								<?php
							}
                            }
							?>

                            <div class="article-text-large">
		                        <?php
		                        echo wpautop($post->post_excerpt);
		                        //								the_excerpt();
		                        ?>
                            </div>

							<?php the_content(); ?>

                            <?php
                            $cat = get_category(get_field("categoria"));
                            if($cat->slug == "video-del-giorno"){ ?>
                                <a class="btn btn-primary mb-2 mt-2" href="<?php echo get_category_link($cat); ?>">Guarda tutti i video</a>
                            <?php } ?>


                            <div class="article-footer">
								<?php get_template_part("template-parts/common/share-button", "article"); ?>
                            </div><!-- /article-footer -->


	                        <?php get_template_part("template-parts/single/licenza"); ?>

                            <?php echo do_shortcode(get_field("shortcode_after_content", "option")); ?>

                            <div class="article-comments">
                                <!-- /DISQUS -->
                            </div><!-- /article-comments -->
                        </article>

                    </div><!-- /col-lg-6 -->

                    <div class="col-lg-4">
                        <aside role="complementary" class="article-aside sticky-sidebar">
                            <?php
                            get_template_part("template-parts/banner/aside", "post");

                            if(!in_category("wayweek"))
                                get_template_part("template-parts/single/related", "aside");

                            ?>
		                    <?php
		                    $box_informativi = get_field("box_informativo");
		                    if($box_informativi){
			                    foreach ($box_informativi as $box){
				                    ?>
                                    <div class="card card-border mb-2">
                                        <div class="card-body">
                                            <h3 class="title-bg title-icon">
							                    <?php  if($box["icona_box"]){ ?>
                                                    <svg class="<?php echo $box["icona_box"]; ?>"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#<?php echo $box["icona_box"]; ?>"></use></svg>
							                    <?php }  ?>
                                                <span><?php echo $box["titolo_box"]; ?></span>
                                            </h3>
                                            <ul class="list-description">
							                    <?php foreach ($box["attributi"] as $attributo){ ?>
                                                    <li>
                                                        <span><?php echo $attributo["nome"]; ?></span>
                                                        <p><?php echo $attributo["valore"]; ?></p>
                                                    </li>
							                    <?php } ?>
                                            </ul>
                                        </div><!-- /card-body -->
                                    </div><!-- /card-border -->
				                    <?php
			                    }
		                    }

		                    ?>
                        </aside>
                    </div><!-- /col-lg-4 -->
                </div><!-- /row -->
	            <?php
	            get_template_part("template-parts/single/map",  get_post_type());
	            ?>

            </div><!-- /container -->
        </main>


		<?php
	}
}
?>
<?php
if(!in_category("wayweek"))
    get_template_part("template-parts/single/related", "post");
?>

<?php
get_footer();
