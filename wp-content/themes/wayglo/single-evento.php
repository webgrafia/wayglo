<?php
/**
 * The main template file
 */

get_header();

if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();

		$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'apertura');
		?>

        <section class="section section-hero">
            <div class="hero-simple">
                <div class="hero-simple-bg" <?php if($featured_img_url){ ?> style="background-image: url('<?php echo $featured_img_url; ?>');" <?php } ?>></div>
                <div class="hero-simple-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 offset-lg-6">
	                            <?php wayglo_cat( "btn btn-white btn-sm text-uppercase" ); ?>
                                <h1><?php the_title(); ?></h1>
                                <p><?php the_excerpt(); ?></p>
                                <div class="row data-evento">
                                    <div class="col-lg-6">
                                        <div class="text-icon-normal">
                                            <svg class="svg-calendar-02"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-calendar-02"></use></svg>
                                            <div class="text-icon-normal-content">
                                                <?php if(get_field("data_inizio")) {
	                                                echo '<span>';
	                                                if(get_field("data_fine"))
    	                                                echo 'da ';
	                                                echo get_field( "data_inizio" ) . '</span>';
                                                    }
                                                    ?>
	                                            <?php if(get_field("data_fine")) echo '<span>a '.get_field("data_fine").'</span>'; ?>
                                            </div><!-- /text-icon-normal-content -->
                                        </div><!-- /text-icon-normal -->
                                    </div><!-- /col-lg-6 -->
                                    <?php if(get_field("ora_inizio") ){ ?>
                                    <div class="col-lg-6">
                                        <div class="text-icon-normal">
                                            <svg class="svg-clock"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-clock"></use></svg>
                                            <div class="text-icon-normal-content">
	                                            <?php if(get_field("ora_inizio")) echo '<span>dalle ore '.get_field("ora_inizio").'</span>'; ?>
	                                            <?php if(get_field("ora_fine")) echo '<span>alle ore '.get_field("ora_fine").'</span>'; ?>
                                            </div><!-- /text-icon-normal-content -->
                                        </div><!-- /text-icon-normal -->
                                    </div><!-- /col-lg-6 -->
                                    <?php } ?>
                                </div><!-- /row -->
                            </div><!-- /col-lg-6 -->
                        </div><!-- /row -->
                    </div><!-- /container -->
                </div><!-- /hero-simple-content -->
            </div><!-- /hero-simple -->
        </section><!-- /section -->



        <main role="main" class="article-wrapper bg-white">
            <div class="container">

                <div class="row sticky-sidebar-container">
                    <div class="col-lg-2">
                        <aside role="complementary" class="article-aside sticky-sidebar">
	                        <?php get_template_part("template-parts/common/share-button", "aside"); ?>
                            <hr/>
                            <div class="tags pr-2">
	                            <?php wayglo_tags($post); ?>
                            </div><!-- /tags -->
                        </aside>
                    </div><!-- /col-lg-2 -->
                    <div class="col-lg-6">
                        <article class="article-container">
                            <?php the_content(); ?>
                        </article>

	                    <?php get_template_part("template-parts/single/licenza"); ?>

	                    <?php echo do_shortcode(get_field("shortcode_after_content", "option")); ?>

                    </div><!-- /col-lg-6 -->
                    <div class="col-lg-3 offset-lg-1">
                        <aside role="complementary" class="article-aside sticky-sidebar">
                            <?php
                            $box_informativi = get_field("box_informativo");
                            if($box_informativi){
                                foreach ($box_informativi as $box){
                                    ?>
                                    <div class="card card-border mb-2">
                                        <div class="card-body">
                                            <h3 class="title-bg title-icon">
                                                <?php  if($box["icona_box"]){ ?>
                                                <svg class="<?php echo $box["icona_box"]; ?>"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#<?php echo $box["icona_box"]; ?>"></use></svg>
                                                <?php }  ?>
                                                <span><?php echo $box["titolo_box"]; ?></span>
                                            </h3>
                                            <ul class="list-description">
	                                            <?php foreach ($box["attributi"] as $attributo){ ?>
                                                <li>
                                                    <span><?php echo $attributo["nome"]; ?></span>
                                                    <p><?php echo $attributo["valore"]; ?></p>
                                                </li>
                                                <?php } ?>
                                            </ul>
                                        </div><!-- /card-body -->
                                    </div><!-- /card-border -->
                                    <?php
                                }
                            }

                            ?>
                        </aside>
                    </div><!-- /col-lg-4 -->
                </div><!-- /row -->

	            <?php
	            get_template_part("template-parts/single/map",  get_post_type());
	            ?>

            </div><!-- /container -->
        </main>
		<?php
	}
}
?>
<?php
//get_template_part("template-parts/single/related",  "evento");
?>

<?php
get_footer();
