<?php
/**
 * The main template file
 */

get_header();

if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
		$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'apertura');

		?>


        <main role="main" class="article-wrapper bg-white">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="breadcrumbs">
                            <a href="<?php echo home_url(); ?>">Home</a>
                            <span class="divider">/</span>
                            <span><?php the_title(); ?></span>
                        </div><!-- /breadcrumb -->
                        <div class="article-title">
                            <h1><?php the_title(); ?></h1>
                            <p class="article-subtitle"></p>
                        </div><!-- /article-title -->
                        <article class="article-container">
	                        <?php
	                        if($featured_img_url){
		                        ?>
                                <img src="<?php echo $featured_img_url; ?>">
		                        <?php
	                        }
	                        ?>
                            <?php the_content(); ?>
                        </article>
                    </div><!-- /col-lg-6 -->

                </div><!-- /row -->
            </div><!-- /container -->
        </main>


		<?php
	}
}
?>

<?php
get_footer();
