<?php
/**
 * The main template file
 */

get_header();
if ( have_posts() ) {
	$c=0;
	$banner=1;
	while ( have_posts() ) {
		the_post();

		if($c == 0){
			get_template_part("template-parts/single/hero");
		}else{
			// apro la sezione
			if(($c == 1) || ($c == 5)) {
				?>
                <section class="section mb-3">
                <div class="container">
                <div class="row sticky-sidebar-container">
                <div class="col-lg-8">
				<?php
			}

			get_template_part("template-parts/single/box-article-horizontal");


			// chiudo la sezione
			if(($c == 4) || ($c == count($posts)-1)) {
				?>

                </div><!-- /col-lg-8 -->
                <div class="col-lg-4">
                    <aside role="complementary" class="section-aside sticky-sidebar">
						<?php
						get_template_part("template-parts/banner/aside", $banner);
						?>
                    </aside>
                </div><!-- /col-lg-4 -->
                </div><!-- /row -->
                </div><!-- /container -->
                </section><!-- /section -->

                <section class="section section-stage mb-3">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
								<?php
								get_template_part("template-parts/banner/archive", $banner);
								?>
                            </div><!-- /col-12 -->
                        </div><!-- /row -->
                    </div><!-- /container -->
                </section><!-- /section -->

				<?php
			}
			?>



			<?php

		}
		$c++;
	}

}else{

	get_template_part("template-parts/common/404");

}
?>
<?php
get_footer();
