<?php
require get_template_directory() . '/inc/acf.php';
require get_template_directory() . '/inc/cpt.php';
require get_template_directory() . '/inc/filters.php';
require get_template_directory() . '/inc/utils.php';



/**
 * Register navigation menus uses wp_nav_menu in five places.
 */
function wayglo_menus() {

	$locations = array(
		'primary'  => __( 'Main Menu', 'wayglo' ),
		'footer1' => __( 'Column 1 footer', 'wayglo' ),
		'footer2' => __( 'Column 2 footer', 'wayglo' ),
		'footer3' => __( 'Column 3 footer', 'wayglo' ),
		'footer4' => __( 'Column 4 footer', 'wayglo' ),
		'footer5' => __( 'Column 5 footer', 'wayglo' ),
		'underfooter' => __( 'Menu under footer', 'wayglo' ),
	);

	register_nav_menus( $locations );
}

add_action( 'init', 'wayglo_menus' );

/*
 * disabilito gutenberg
 */
add_filter('use_block_editor_for_post_type', '__return_false');

if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 300, 300, true ); // default Featured Image dimensions (cropped)
	add_image_size( 'apertura', 2000, 1200, true ); // Hard Crop Mode

	add_image_size( 'card', 500, 330, true ); // Hard Crop Mode
	add_image_size( 'card-small', 500, 250, true ); // Hard Crop Mode

	add_image_size( 'big-square', 700, 700, true ); // Hard Crop Mode
	add_image_size( 'book', 500, 780, true ); // Hard Crop Mode
	add_image_size( 'box-article', 500, 385, true ); // Hard Crop Mode

	add_image_size( 'slider-small', 300, 250, true ); // Hard Crop Mode
	add_image_size( 'thin', 1000, 430, true ); // Hard Crop Mode


	add_image_size( 'palco-small', 500, 330, true ); // Hard Crop Mode
	add_image_size( 'palco-medium', 780, 400, true ); // Hard Crop Mode
	add_image_size( 'palco-large', 500, 780, true ); // Hard Crop Mode
	add_image_size( 'mosaico', 1000, 650, true ); // Hard Crop Mode

	add_image_size( 'slider-small', 1800, 1150, true ); // Hard Crop Mode



}
