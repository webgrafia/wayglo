<?php
/**
 * The main template file
 */

get_header();

if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
		?>
        <main role="main" class="article-wrapper bg-white">
            <div class="container">
                <div class="row sticky-sidebar-container">
                    <div class="col-lg-2">
                        <aside role="complementary" class="article-aside sticky-sidebar">
                            <div class="text-large-right-bg bg-blue">
                                <span>Accadde oggi</span>
                                <p class="special-data">
	                                <?php
	                                $data_almanacco = get_field("data_almanacco");
	                                $data_almanacco_testo = get_field("data_almanacco_testo");
	                                if($data_almanacco_testo){
		                                echo $data_almanacco_testo;
	                                }else if($data_almanacco){
		                                echo $data_almanacco;
	                                }
	                                ?>
                                </p>
                            </div><!-- /text-large-right-bg -->
							<?php get_template_part("template-parts/common/share-button", "aside"); ?>
                            <hr/>
                            <!--
                            <div class="text-tiny-icon mb-1">
                                <svg class="svg-calendar"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-calendar"></use></svg>
                                <p><?php the_date(); ?></p>
                            </div> -->
                            <div class="tags pr-2">
								<?php wayglo_tags($post); ?>
                            </div><!-- /tags -->

							<?php
							$fonte = get_field("fonte_notizia");
							if($fonte){
								?>
                                <hr/>
                                <div class="smaller-text">
                                    <span class="d-block">fonte:</span>
                                    <strong><?php echo $fonte; ?></strong>
                                </div><!-- /smaller-text -->
								<?php
							}
							?>
                        </aside>
                    </div><!-- /col-lg-2 -->
                    <div class="col-lg-6">
                        <div class="breadcrumbs">
	                        <?php wayglo_cat( "btn btn-white text-uppercase" ); ?>
                        </div><!-- /breadcrumb -->
                        <div class="article-title">
                            <h1><?php the_title(); ?></h1>
                            <p class="article-subtitle"></p>
                        </div><!-- /article-title -->
                        <article class="article-container">
                            <!--
                            <div class="article-text-large">
								<?php
//                              echo wpautop($post->post_excerpt);
//								the_excerpt();
								?>
                            </div>
                            //-->
							<?php
							if(has_post_thumbnail()){
								?>
                                <div class="featured">
	                                <?php
	                                $img = get_the_post_thumbnail(get_the_ID(),"apertura");
	                                $featured_full = get_the_post_thumbnail_url(get_the_ID(),'full');
	                                if($img){
		                                $featured = '<a href="'.$featured_full.'">'.$img.'</a>';
		                                if ( function_exists( 'slb_activate' ) ) {
			                                $featured = slb_activate( $featured );
		                                }
		                                echo $featured;
	                                }
	                                ?>
									<?php
									    echo "<p>";
										the_post_thumbnail_caption(get_the_ID());
										echo "</p>";

									?>
                                </div>
								<?php
							}
							?>

							<?php the_content(); ?>

                            <div class="article-footer">
								<?php get_template_part("template-parts/common/share-button", "article"); ?>
                            </div><!-- /article-footer -->

	                        <?php get_template_part("template-parts/single/licenza"); ?>

	                        <?php echo do_shortcode(get_field("shortcode_after_content", "option")); ?>

                            <div class="article-comments">
                                <!-- /DISQUS -->
                            </div><!-- /article-comments -->
                        </article>
                    </div><!-- /col-lg-6 -->

                    <div class="col-lg-4">
                        <aside role="complementary" class="article-aside sticky-sidebar">
							<?php
							get_template_part("template-parts/banner/aside", "almanacco");

							$cat = get_category(get_field("categoria"));

							// recupero per categoria
							$args = array(
								'posts_per_page'=>3,
								'category__in'   => wp_get_post_categories(get_the_ID()),
								'post_type' => get_post_type(),
								'post__not_in' => array(get_the_ID()),
								'meta_query' => array(
									array(
										'type' => 'NUMERIC',
										'key'=>'dayofyear',
										'value'=> date("z"),
										'compare'=>'>=',
									),
								),
								'meta_key' => "dayofyear",
								'orderby' => "meta_value_num",
								'order' => "ASC",
							);

							$related_almanacco = get_posts($args);
                            /*
							// recupero almanacchi recenti
							$args = array(
								'posts_per_page'=>3,
								'post_type' => get_post_type(),
								'post__not_in' => array(get_the_ID())
							);
							$related_almanacco = get_posts($args);
                            */
							if($related_almanacco){
								foreach ( $related_almanacco as $item ) {
									get_template_part("template-parts/almanacco/card", "aside");

								}
							}
							?>
                        </aside>
                    </div><!-- /col-lg-4 -->
                </div><!-- /row -->
            </div><!-- /container -->
        </main>


		<?php
	}
}
?>
<?php
//get_template_part("template-parts/single/related", "almanacco");
?>

<?php
get_footer();
