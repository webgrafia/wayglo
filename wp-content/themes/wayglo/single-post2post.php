<?php
/**
 * The main template file
 */

get_header();

if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
		?>
        <main role="main" class="article-wrapper bg-white">
            <div class="container">
                <div class="row sticky-sidebar-container">
                    <div class="col-lg-2">
                        <aside role="complementary" class="article-aside sticky-sidebar">

                            <?php
                            if(get_field("social")){
                            ?>
                            <div class="text-small-svg-icon">
                                <?php
                                get_template_part("template-parts/social/icon", sanitize_title(get_field("social")));
                                ?>
                                <p>da <?php echo get_field("social"); ?></p>
                            </div><!-- /text-small-svg-icon -->
                            <?php
                            }
                            ?>
							<?php get_template_part("template-parts/common/share-button", "aside"); ?>
                            <hr/>
                            <div class="text-tiny-icon mb-1">
                                <svg class="svg-calendar"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-calendar"></use></svg>
                                <p><?php the_date(); ?></p>
                            </div><!-- /text-tiny-icon -->
                            <div class="tags pr-2">
								<?php wayglo_tags($post); ?>
                            </div><!-- /tags -->

                        </aside>
                    </div><!-- /col-lg-2 -->
                    <div class="col-lg-6">
                        <div class="breadcrumbs">
	                        <?php wayglo_cat(); ?>
                            <span class="divider">/</span>
                            <span>Post2Post</span>
                        </div><!-- /breadcrumb -->
                        <div class="article-title">
                            <h1><?php the_title(); ?></h1>
                            <p class="article-subtitle"></p>
                        </div><!-- /article-title -->
                        <article class="article-container">
                            <div class="article-text-large">
								<?php
								the_excerpt();
								?>
                            </div>
	                        <?php
	                        $img = get_the_post_thumbnail(get_the_ID(),"book");
	                        $featured_full = get_the_post_thumbnail_url(get_the_ID(),'full');
	                        if($img){
		                        $featured = '<a href="'.$featured_full.'">'.$img.'</a>';
		                        if ( function_exists( 'slb_activate' ) ) {
			                        $featured = slb_activate( $featured );
		                        }
		                        echo $featured;
	                        }
	                        ?>

							<?php the_content(); ?>

                            <div class="article-footer">
								<?php get_template_part("template-parts/common/share-button", "article"); ?>
                            </div><!-- /article-footer -->

	                        <?php get_template_part("template-parts/single/licenza"); ?>

	                        <?php echo do_shortcode(get_field("shortcode_after_content", "option")); ?>

                            <div class="article-comments">
                                <!-- /DISQUS -->
                            </div><!-- /article-comments -->
                        </article>
                    </div><!-- /col-lg-6 -->

                    <div class="col-lg-4">
                        <aside role="complementary" class="article-aside sticky-sidebar">
                            <?php
                            get_template_part("template-parts/banner/aside", "post2post");
                            ?>
                        </aside>
                    </div><!-- /col-lg-4 -->
                </div><!-- /row -->
            </div><!-- /container -->
        </main>


		<?php
	}
}
?>
<?php
get_template_part("template-parts/single/related", "post2post");
?>

<?php
get_footer();
