<?php

add_filter( 'wp_enqueue_scripts', 'change_default_jquery', PHP_INT_MAX );

function change_default_jquery( ){

	if( !is_admin() ) {
		wp_deregister_script( 'jquery' );
		wp_register_script( 'jquery', get_stylesheet_directory_uri() . '/assets/js/vendor/jquery-3.4.1.min.js', false, 1, true );
		wp_enqueue_script( 'jquery' );
	}
}

function wg_acf_google_map_api( $api ){
	$key = get_field("gmap_api", "options");
	$api['key'] = $key;
	return $api;
}
add_filter('acf/fields/google_map/api', 'wg_acf_google_map_api');


function wayglo_class_on_a_menu($classes, $item, $args)
{
	if(in_array("menu-item-has-children", $item->classes))
		$classes['class'] = "trigger-children";

	return $classes;
}

add_filter('nav_menu_link_attributes', 'wayglo_class_on_a_menu', 1, 3);




/**
 * riscrivo la gallery nativa
 *
 * @param $string
 * @param $attr
 *
 * @return string
 */
add_filter('post_gallery','wayglo_gallery',10,2);
function wayglo_gallery($string,$attr){


	$output = '<div class="article-gallery">';
	$output .= '<div class="owl-carousel owl-theme carousel-gallery lightgallery">';
	$posts = get_posts(array('include' => $attr['ids'],'post_type' => 'attachment'));

	$ids=explode(",", $attr['ids']);
	// ordino l'array per id
	$orderpost = array();
	foreach ( $posts as $post ) {
		$key = array_search($post->ID, $ids);
		$orderpost[$key] = $post;
	}
	ksort($orderpost);

	$i=0;

	foreach($orderpost as $imagePost){
		$i++;
	$output .= '<div class="item">
                      <a href="'.wp_get_attachment_image_src($imagePost->ID, 'apertura')[0].'" data-sub-html=".caption'.$i.'">
                        '.wp_get_attachment_image($imagePost->ID, "large").'
                         <div class="caption'.$i.' d-none">
                         <h4>'.$imagePost->post_title.'</h4>
                         <p>'.$imagePost->post_excerpt.'</p>
                         </div>
                      </a>
                    </div>';
	}
	$output .= "</div>";
	$output .= "</div>";
	return $output;
}

// estendo la categoria agli altri post type

add_filter('pre_get_posts', 'wayglo_query_post_type');
function wayglo_query_post_type($query) {
	if( is_category() || is_tag() ) {
		$post_type = get_query_var('post_type');
		if($post_type)
			$post_type = $post_type;
		else
			$post_type = array('post', 'scheda', 'almanacco', 'book', 'evento', 'post2post', 'nav_menu_item'); // don't forget nav_menu_item to allow menus to work!
		$query->set('post_type',$post_type);
		return $query;
	}
}


/**
 * salvo il dato del giorno dell'anno in base alla data inserita
 */
add_action('acf/save_post', 'wg_acf_save_post');
function wg_acf_save_post( $post_id ) {
	$data_almanacco = get_field('data_almanacco', $post_id);
	if( $data_almanacco ) {
		$date = date_create_from_format('d/m/Y', $data_almanacco);
		if($date){
			$fieldsanno  = explode("/", $data_almanacco);
			$year = $fieldsanno[2];
			$dateofyear = $date->format('z');
			if($dateofyear >= 60){
				if(wg_is_bisestile($year)){
					$dateofyear--;
				}
			}
			update_post_meta($post_id, "dayofyear", $dateofyear);
		}
	}
}

function wg_is_bisestile($anno){
	if($anno == ""){
		return false;
	}else if(($anno % 400) == 0){
		return true;		//Is LeapYear
	}else if(($anno % 100) == 0){
		return false;		//Not a LeapYear
	}else if(($anno % 4) == 0){
		return true;		//Is LeapYear
	}else{
		return false;		//Not LeapYear
	}
}

/**
 * ordino per data la categoria almanacco e il post type archive
 */

function wg_custom_sort_almanacco( $query ) {
	if (
	(is_category( 'almanacco' ) || is_post_type_archive("almanacco"))
		&& !is_admin() // and we're not in the admin
		&& $query->is_main_query() // and this is WP's main query
	) {

		$ids = array();

		$countgiorno = date("z");
		if(wg_is_bisestile(date("Y")) && $countgiorno >= 60 )
			$countgiorno--;
		// futuri e poi passati
		$query1 = new WP_Query([
			'posts_per_page' => -1,
			'post_type' => 'almanacco',
			'meta_query' => array(
				array(
					'type' => 'NUMERIC',
					'key'=>'dayofyear',
					'value'=> $countgiorno,
					'compare'=>'>=',
				),
			),
			'meta_key' => "dayofyear",
			'orderby' => "meta_value_num",
			'order' => "ASC",
		]);


		foreach ( $query1->posts as $post ) {
			//echo "<hr>".get_post_meta($post->ID, "dayofyear", true).") ".$post->post_title;
			$ids[]=$post->ID;
		}

		$query2 = new WP_Query([
			'posts_per_page' => -1,
			'post_type' => 'almanacco',
			'meta_query' => array(
				array(
					'type' => 'NUMERIC',
					'key'=>'dayofyear',
					'value'=> $countgiorno,
					'compare'=>'<',
				),
			),
			'meta_key' => "dayofyear",
			'orderby' => "meta_value_num",
			'order' => "ASC",
		]);

		foreach ( $query2->posts as $post ) {
		//  	echo "<hr>".get_post_meta($post->ID, "dayofyear", true).") ".$post->post_title;
			$ids[]=$post->ID;
		}

		wp_reset_query();

		$query->set( 'post__in', $ids );
		$query->set( 'orderby', 'post__in' );

/*
		if(!$_GET["old"]){
			$meta_query = array(
				array(
					'key'=>'dayofyear',
					'value'=> date("z"),
					'compare'=>'>=',
				),
			);
			$order = "ASC";
		}else{
			$meta_query = array(
				array(
					'key'=>'dayofyear',
					'value'=> date("z"),
					'compare'=>'<',
				),
			);
			$order = "DESC";
		}
		$query->set('meta_query',$meta_query);
		$query->set( 'orderby', 'meta_value' );
		$query->set( 'meta_key', 'dayofyear' );
		$query->set( 'order', $order );
*/

	}

}
add_action( 'pre_get_posts', 'wg_custom_sort_almanacco' );