<?php
// Register Custom Post Type
function wayglo_custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Schede', 'Post Type General Name', 'wayglo' ),
		'singular_name'         => _x( 'Scheda', 'Post Type Singular Name', 'wayglo' ),
		'menu_name'             => __( 'Schede', 'wayglo' ),
		'name_admin_bar'        => __( 'Scheda', 'wayglo' ),
		'archives'              => __( 'Item Archives', 'wayglo' ),
		'attributes'            => __( 'Item Attributes', 'wayglo' ),
		'parent_item_colon'     => __( 'Parent Item:', 'wayglo' ),
		'all_items'             => __( 'All Items', 'wayglo' ),
		'add_new_item'          => __( 'Add New Item', 'wayglo' ),
		'add_new'               => __( 'Add New', 'wayglo' ),
		'new_item'              => __( 'New Item', 'wayglo' ),
		'edit_item'             => __( 'Edit Item', 'wayglo' ),
		'update_item'           => __( 'Update Item', 'wayglo' ),
		'view_item'             => __( 'View Item', 'wayglo' ),
		'view_items'            => __( 'View Items', 'wayglo' ),
		'search_items'          => __( 'Search Item', 'wayglo' ),
		'not_found'             => __( 'Not found', 'wayglo' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'wayglo' ),
		'featured_image'        => __( 'Featured Image', 'wayglo' ),
		'set_featured_image'    => __( 'Set featured image', 'wayglo' ),
		'remove_featured_image' => __( 'Remove featured image', 'wayglo' ),
		'use_featured_image'    => __( 'Use as featured image', 'wayglo' ),
		'insert_into_item'      => __( 'Insert into item', 'wayglo' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'wayglo' ),
		'items_list'            => __( 'Items list', 'wayglo' ),
		'items_list_navigation' => __( 'Items list navigation', 'wayglo' ),
		'filter_items_list'     => __( 'Filter items list', 'wayglo' ),
	);
	$args = array(
		'label'                 => __( 'Scheda', 'wayglo' ),
		'description'           => __( 'Scheda', 'wayglo' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'custom-fields' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-table-col-before',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
	);
	register_post_type( 'scheda', $args );



	$labels = array(
		'name'                  => _x( 'Almanacco', 'Post Type General Name', 'wayglo' ),
		'singular_name'         => _x( 'Almanacco', 'Post Type Singular Name', 'wayglo' ),
		'menu_name'             => __( 'Almanacco', 'wayglo' ),
		'name_admin_bar'        => __( 'Almanacco', 'wayglo' ),
		'archives'              => __( 'Item Archives', 'wayglo' ),
		'attributes'            => __( 'Item Attributes', 'wayglo' ),
		'parent_item_colon'     => __( 'Parent Item:', 'wayglo' ),
		'all_items'             => __( 'All Items', 'wayglo' ),
		'add_new_item'          => __( 'Add New Item', 'wayglo' ),
		'add_new'               => __( 'Add New', 'wayglo' ),
		'new_item'              => __( 'New Item', 'wayglo' ),
		'edit_item'             => __( 'Edit Item', 'wayglo' ),
		'update_item'           => __( 'Update Item', 'wayglo' ),
		'view_item'             => __( 'View Item', 'wayglo' ),
		'view_items'            => __( 'View Items', 'wayglo' ),
		'search_items'          => __( 'Search Item', 'wayglo' ),
		'not_found'             => __( 'Not found', 'wayglo' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'wayglo' ),
		'featured_image'        => __( 'Featured Image', 'wayglo' ),
		'set_featured_image'    => __( 'Set featured image', 'wayglo' ),
		'remove_featured_image' => __( 'Remove featured image', 'wayglo' ),
		'use_featured_image'    => __( 'Use as featured image', 'wayglo' ),
		'insert_into_item'      => __( 'Insert into item', 'wayglo' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'wayglo' ),
		'items_list'            => __( 'Items list', 'wayglo' ),
		'items_list_navigation' => __( 'Items list navigation', 'wayglo' ),
		'filter_items_list'     => __( 'Filter items list', 'wayglo' ),
	);
	$args = array(
		'label'                 => __( 'Almanacco', 'wayglo' ),
		'description'           => __( 'Almanacco', 'wayglo' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor',  'excerpt', 'thumbnail', 'custom-fields' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-calendar-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
	);
	register_post_type( 'almanacco', $args );

	$labels = array(
		'name'                  => _x( 'Book', 'Post Type General Name', 'wayglo' ),
		'singular_name'         => _x( 'Book', 'Post Type Singular Name', 'wayglo' ),
		'menu_name'             => __( 'Book', 'wayglo' ),
		'name_admin_bar'        => __( 'Book', 'wayglo' ),
		'archives'              => __( 'Item Archives', 'wayglo' ),
		'attributes'            => __( 'Item Attributes', 'wayglo' ),
		'parent_item_colon'     => __( 'Parent Item:', 'wayglo' ),
		'all_items'             => __( 'All Items', 'wayglo' ),
		'add_new_item'          => __( 'Add New Item', 'wayglo' ),
		'add_new'               => __( 'Add New', 'wayglo' ),
		'new_item'              => __( 'New Item', 'wayglo' ),
		'edit_item'             => __( 'Edit Item', 'wayglo' ),
		'update_item'           => __( 'Update Item', 'wayglo' ),
		'view_item'             => __( 'View Item', 'wayglo' ),
		'view_items'            => __( 'View Items', 'wayglo' ),
		'search_items'          => __( 'Search Item', 'wayglo' ),
		'not_found'             => __( 'Not found', 'wayglo' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'wayglo' ),
		'featured_image'        => __( 'Featured Image', 'wayglo' ),
		'set_featured_image'    => __( 'Set featured image', 'wayglo' ),
		'remove_featured_image' => __( 'Remove featured image', 'wayglo' ),
		'use_featured_image'    => __( 'Use as featured image', 'wayglo' ),
		'insert_into_item'      => __( 'Insert into item', 'wayglo' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'wayglo' ),
		'items_list'            => __( 'Items list', 'wayglo' ),
		'items_list_navigation' => __( 'Items list navigation', 'wayglo' ),
		'filter_items_list'     => __( 'Filter items list', 'wayglo' ),
	);
	$args = array(
		'label'                 => __( 'Book', 'wayglo' ),
		'description'           => __( 'Book', 'wayglo' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'custom-fields' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-book-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
	);
	register_post_type( 'book', $args );

	$labels = array(
		'name'                  => _x( 'Evento', 'Post Type General Name', 'wayglo' ),
		'singular_name'         => _x( 'Evento', 'Post Type Singular Name', 'wayglo' ),
		'menu_name'             => __( 'Eventi', 'wayglo' ),
		'name_admin_bar'        => __( 'Evento', 'wayglo' ),
		'archives'              => __( 'Item Archives', 'wayglo' ),
		'attributes'            => __( 'Item Attributes', 'wayglo' ),
		'parent_item_colon'     => __( 'Parent Item:', 'wayglo' ),
		'all_items'             => __( 'All Items', 'wayglo' ),
		'add_new_item'          => __( 'Add New Item', 'wayglo' ),
		'add_new'               => __( 'Add New', 'wayglo' ),
		'new_item'              => __( 'New Item', 'wayglo' ),
		'edit_item'             => __( 'Edit Item', 'wayglo' ),
		'update_item'           => __( 'Update Item', 'wayglo' ),
		'view_item'             => __( 'View Item', 'wayglo' ),
		'view_items'            => __( 'View Items', 'wayglo' ),
		'search_items'          => __( 'Search Item', 'wayglo' ),
		'not_found'             => __( 'Not found', 'wayglo' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'wayglo' ),
		'featured_image'        => __( 'Featured Image', 'wayglo' ),
		'set_featured_image'    => __( 'Set featured image', 'wayglo' ),
		'remove_featured_image' => __( 'Remove featured image', 'wayglo' ),
		'use_featured_image'    => __( 'Use as featured image', 'wayglo' ),
		'insert_into_item'      => __( 'Insert into item', 'wayglo' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'wayglo' ),
		'items_list'            => __( 'Items list', 'wayglo' ),
		'items_list_navigation' => __( 'Items list navigation', 'wayglo' ),
		'filter_items_list'     => __( 'Filter items list', 'wayglo' ),
	);
	$args = array(
		'label'                 => __( 'Evento', 'wayglo' ),
		'description'           => __( 'Evento', 'wayglo' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'custom-fields' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-clock',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
	);
	register_post_type( 'evento', $args );


	$labels = array(
		'name'                  => _x( 'Post2Post', 'Post Type General Name', 'wayglo' ),
		'singular_name'         => _x( 'Post2Post', 'Post Type Singular Name', 'wayglo' ),
		'menu_name'             => __( 'Post2Post', 'wayglo' ),
		'name_admin_bar'        => __( 'Post2Post', 'wayglo' ),
		'archives'              => __( 'Item Archives', 'wayglo' ),
		'attributes'            => __( 'Item Attributes', 'wayglo' ),
		'parent_item_colon'     => __( 'Parent Item:', 'wayglo' ),
		'all_items'             => __( 'All Items', 'wayglo' ),
		'add_new_item'          => __( 'Add New Item', 'wayglo' ),
		'add_new'               => __( 'Add New', 'wayglo' ),
		'new_item'              => __( 'New Item', 'wayglo' ),
		'edit_item'             => __( 'Edit Item', 'wayglo' ),
		'update_item'           => __( 'Update Item', 'wayglo' ),
		'view_item'             => __( 'View Item', 'wayglo' ),
		'view_items'            => __( 'View Items', 'wayglo' ),
		'search_items'          => __( 'Search Item', 'wayglo' ),
		'not_found'             => __( 'Not found', 'wayglo' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'wayglo' ),
		'featured_image'        => __( 'Featured Image', 'wayglo' ),
		'set_featured_image'    => __( 'Set featured image', 'wayglo' ),
		'remove_featured_image' => __( 'Remove featured image', 'wayglo' ),
		'use_featured_image'    => __( 'Use as featured image', 'wayglo' ),
		'insert_into_item'      => __( 'Insert into item', 'wayglo' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'wayglo' ),
		'items_list'            => __( 'Items list', 'wayglo' ),
		'items_list_navigation' => __( 'Items list navigation', 'wayglo' ),
		'filter_items_list'     => __( 'Filter items list', 'wayglo' ),
	);
	$args = array(
		'label'                 => __( 'Post2Post', 'wayglo' ),
		'description'           => __( 'Post2Post', 'wayglo' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'custom-fields' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-format-chat',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
	);
	register_post_type( 'post2post', $args );

}
add_action( 'init', 'wayglo_custom_post_type', 0 );

