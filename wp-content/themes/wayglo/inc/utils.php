<?php


function wayglo_tags($post, $class="btn btn-xs btn-primary"){
	$tags = wp_get_post_tags($post->ID); //this is the adjustment, all the rest is bhlarsen
	foreach ( $tags as $tag ) {
		$tag_link = get_tag_link( $tag->term_id );
		echo  "<a class='".$class."'  href='{$tag_link}' title='{$tag->name} Tag'>";
		echo  "{$tag->name}</a> ";
	}
}

function wayglo_cat($class = "", $item = "", $exclude_color=false){
	global $post;
	if($item) $post = $item;
	echo get_wayglo_cat($class, $post, $exclude_color);
}


function get_wayglo_cat($class, $item = "", $exclude_color=false){
	global $post;
	if($item) $post = $item;
	$cat = get_category(get_field("categoria", $post));
	if($cat){
		$colore_occhiello_categoria = get_field("colore_occhiello_categoria", $cat);
		if($colore_occhiello_categoria && !$exclude_color)
			return '<a class="'.$class.'"  style="background-color:'. $colore_occhiello_categoria.';border-color:'. $colore_occhiello_categoria.';" href="'.get_category_link($cat).'">'.$cat->name.'</a>';

		return '<a class="'.$class.'" href="'.get_category_link($cat).'">'.$cat->name.'</a>';
	}
}

function display_dot($string){
	// estraggo la data esplodendo lo spazio
	$array_string = explode(" ", trim($string));

	if(is_numeric($array_string[count($array_string)-2])){
		$giorno = $array_string[count($array_string)-2];
		$mese = "";
		switch (strtolower($array_string[count($array_string)-1])){
			case "gennaio":
				$mese = "january";
				break;
			case "febbraio":
			case "febraio":
				$mese = "february";
				break;
			case "marzo":
				$mese = "march";
				break;
			case "aprile":
				$mese = "april";
				break;
			case "maggio":
				$mese = "may";
				break;
			case "giugno":
				$mese = "june";
				break;
			case "luglio":
				$mese = "july";
				break;
			case "agosto":
				$mese = "august";
				break;
			case "settembre":
				$mese = "september";
				break;
			case "ottobre":
				$mese = "october";
				break;
			case "novembre":
				$mese = "november";
				break;
			case "dicembre":
				$mese = "december";
				break;
		}

		if($mese){
			$format_date = $giorno." ".$mese." ".date("Y");
			$time = strtotime($format_date);
			if(date("d-m-Y", $time) == date("d-m-Y"))
				echo '<span class="dot"></span>';
		}
	}
}
