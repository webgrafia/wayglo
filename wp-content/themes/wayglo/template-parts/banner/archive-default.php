<?php
$img_banner = get_field("img_banner_archive", "options");
$link_banner = get_field("url_banner_archive", "options");
if($img_banner){
	?>
    <section class="section section-stage mb-3">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="box box-adv">
                        <a href="<?php echo $link_banner; ?>">
                            <img src="<?php echo $img_banner; ?>">
                        </a>
                    </div><!-- /box-adv -->
                </div><!-- /col-12 -->
            </div><!-- /row -->
        </div><!-- /container -->
    </section><!-- /section -->
	<?php
}