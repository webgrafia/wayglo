<?php
$img_banner = get_field("img_banner_sidebar", "options");
$link_banner = get_field("url_banner_sidebar", "options");
if($img_banner){
	?>
    <div class="box box-adv mb-2">
        <a href="<?php echo $link_banner; ?>">
            <img src="<?php echo $img_banner; ?>">
        </a>
    </div><!-- /box-adv -->
	<?php
}