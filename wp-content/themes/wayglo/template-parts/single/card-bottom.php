<?php
global $item;

$related_img_url = get_the_post_thumbnail_url($item->ID,'palco-small');

?>
    <div class="item">
        <div class="box box-simple">
            <div class="box-thumb">
                <img src="<?php echo $related_img_url; ?>" alt="<?php echo esc_attr($item->post_title); ?>" title="<?php echo esc_attr($item->post_title); ?>">
            </div><!-- /box-thumb -->
            <div class="box-content">
                <h3><a href="<?php echo get_permalink($item); ?>"><?php echo $item->post_title; ?></a></h3>
            </div><!-- /box-content -->
        </div><!-- /box-simple -->
    </div>

<?php

