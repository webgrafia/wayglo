<?php
$img_url = get_the_post_thumbnail_url($post->ID,'box-article');
if($post->post_type == "book"){
	$img_url = get_the_post_thumbnail_url($post->ID,'book');
}
?>

<div class="box box-article-horizontal mb-2">
	<div class="box-thumb">
		<a href="<?php the_permalink(); ?>"><img src="<?php echo $img_url; ?>" alt="<?php echo esc_attr($post->post_title); ?>" title="<?php echo esc_attr($post->post_title); ?>"></a>
	</div><!-- /box-thumb -->
	<div class="box-content-wrapper">
		<div class="box-content">
			<?php wayglo_cat("btn btn-white btn-sm text-uppercase mb-1"); ?>
            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
			<?php the_excerpt(); ?>
		</div><!-- /box-content -->
	</div><!-- /box-content-wrapper -->
</div><!-- /box -->
<?php
