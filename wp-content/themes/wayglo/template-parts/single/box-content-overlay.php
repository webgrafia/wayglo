<?php
global $post;

$related_img_url = get_the_post_thumbnail_url($post->ID,'big-square');

?>

    <div class="box box-content-overlay mb-2">
        <div class="box-content">
            <h3><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h3>
            <span class="tag">
                        <?php wayglo_cat( "text-primary" , $post, true); ?>
                      </span><!-- /tag -->
        </div><!-- /box-content -->
        <img src="<?php echo $related_img_url; ?>" alt="<?php echo esc_attr($post->post_title); ?>" title="<?php echo esc_attr($post->post_title); ?>">
    </div><!-- /box-content-overlay -->

<?php

