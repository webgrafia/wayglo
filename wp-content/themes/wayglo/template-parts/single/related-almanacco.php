<?php
global $item;

$cat = get_category(get_field("categoria"));

// recupero per categoria
$args = array(
	'posts_per_page'=>6,
	'category__in'   => wp_get_post_categories(get_the_ID()),
	'post_type' => get_post_type(),
	'post__not_in' => array(get_the_ID())
);

$related = get_posts($args);
/*$tags = wp_get_post_tags(get_the_ID());
if(is_array($tags) && count($tags)){
    $tag = $tags[0];

    // recupero gli altri post_type di questo tag
	$args = array(
	        'tag_id'=>$tag->term_id,
	        'posts_per_page'=>6,
            'post_type' => "any",
            //  'post_type' => get_post_type(),
            'post__not_in' => array(get_the_ID())
	);
    $related = get_posts($args);
*/

    if($related){
?>
        <section class="section section-stage pt-4 pb-3">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="stage-title stage-title-small">
                            <h2>Forse potrebbero interessarti</h2>
                        </div><!-- /stage-title -->
                    </div><!-- /col -->
                </div><!-- /row -->
            </div><!-- /container -->
            <div class="stage-body">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="owl-carousel owl-theme carousel-stage">
                            <?php
                            foreach ( $related as $item ) {
	                            get_template_part("template-parts/single/card", "overlay");
                            } ?>
                            </div><!-- /carousel-stage -->
                        </div><!-- /col -->
                    </div><!-- /row -->
                </div><!-- /container -->
            </div><!-- /stage-body -->
        </section><!-- /section -->

<?php
//}
}