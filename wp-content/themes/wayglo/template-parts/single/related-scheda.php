<?php
global $item;

$cat = get_category(get_field("categoria"));

// recupero per categoria
$args = array(
	'posts_per_page'=>6,
	'category__in'   => wp_get_post_categories(get_the_ID()),
	'post_type' => get_post_type(),
	'post__not_in' => array(get_the_ID())
);

$related = get_posts($args);
/*
// recupero il post tag
$tags = wp_get_post_tags(get_the_ID());
if(is_array($tags) && count($tags)){
	$tag_ids = array();
	foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;

    // recupero gli altri post_type di questo tag
	$args = array(
	        'tag__in'=>$tag_ids,
	        'posts_per_page'=>6,
            'post_type' => get_post_type(),
            'post__not_in' => array(get_the_ID())
	);
    $related = get_posts($args);
*/
    if($related){

?>
<section class="section section-stage pt-4 pb-3 related-bottom">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="stage-title stage-title-small">
					<h2>Leggi anche</h2>
				</div><!-- /stage-title -->
			</div><!-- /col -->
		</div><!-- /row -->
	</div><!-- /container -->
	<div class="stage-body">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="owl-carousel owl-theme carousel-stage">
                        <?php
                        foreach ( $related as $item ) {
	                        get_template_part("template-parts/single/card", "custom");
                        }
                        ?>
                    </div><!-- /carousel-stage -->
				</div><!-- /col -->
			</div><!-- /row -->
		</div><!-- /container -->
	</div><!-- /stage-body -->
</section><!-- /section -->
<?php
//}
}