<?php
if(get_field("abilita_mappa")){

	$posizione = get_field("posizione");
	if($posizione){
	?>
<style>
    .link-website {
        width : 100%;
        overflow:hidden;
        display:inline-block;
        text-overflow: ellipsis;
        white-space: nowrap;
    }
</style>
<div class="row my-2" id="map-wrapper">
	<div class="col-lg-11 offset-lg-1">
		<div class="map-wrapper">
			<div class="map-description">
				<h3><?php echo get_field("titolo_box_mappa") ?></h3>
				<ul class="list-icons">
					<?php if(get_field("telefono")){ ?>
					<li>
						<svg class="svg-phone"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-phone"></use></svg>
						<span><?php echo get_field("telefono") ?></span>
					</li>
					<?php } ?>
					<?php if(get_field("website")){ ?>
					<li>
						<svg class="svg-laptop"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-laptop"></use></svg>
						<span><a class="link-website" href="<?php echo get_field("website") ?>" target="_blank"><?php echo get_field("website") ?></a></span>
					</li>
					<?php } ?>
				</ul>
				<?php if(get_field("indirizzo")){ ?>
                    <div class="box-icon">
                        <svg class="svg-marker"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-marker"></use></svg>
                        <h6>Indirizzo:</h6>
                        <p><?php echo get_field("indirizzo"); ?></p>
                    </div>
				<?php } ?>
			</div><!-- /map-description -->
			<div class="map-container">
				<div id="article-map"></div>
			</div><!-- /map-container -->
		</div><!-- /map-wrapper -->
	</div><!-- /col-10 -->
</div><!-- /row -->
<script type="text/javascript" src="//maps.google.com/maps/api/js?key=<?php echo get_field("gmap_api", "options"); ?>"></script>
<script>
    function initialise() {
        var myLatlng = new google.maps.LatLng(<?php echo $posizione["lat"]; ?>, <?php echo $posizione["lng"]; ?>); // Add the coordinates
        var mapOptions = {
            zoom: 15, // The initial zoom level when your map loads (0-20)
            minZoom: 6, // Minimum zoom level allowed (0-20)
            maxZoom: 17, // Maximum soom level allowed (0-20)
            zoomControl:true, // Set to true if using zoomControlOptions below, or false to remove all zoom controls.
            zoomControlOptions: {
                style:google.maps.ZoomControlStyle.DEFAULT // Change to SMALL to force just the + and - buttons.
            },
            center: myLatlng, // Centre the Map to our coordinates variable
            mapTypeId: google.maps.MapTypeId.ROADMAP, // Set the type of Map
            scrollwheel: false, // Disable Mouse Scroll zooming (Essential for responsive sites!)
            // All of the below are set to true by default, so simply remove if set to true:
            panControl:false, // Set to false to disable
            mapTypeControl:false, // Disable Map/Satellite switch
            scaleControl:false, // Set to false to hide scale
            streetViewControl:false, // Set to disable to hide street view
            overviewMapControl:false, // Set to false to remove overview control
            fullscreenControl: false,
            rotateControl:false // Set to false to disable rotate control
        }
        var map = new google.maps.Map(document.getElementById('article-map'), mapOptions); // Render our map within the empty div

        var image = new google.maps.MarkerImage("<?php echo get_template_directory_uri(); ?>/assets/img/pin.svg", null, null, null, new google.maps.Size(54,72)); // Create a variable for our marker image.

        var marker = new google.maps.Marker({ // Set the marker
            position: myLatlng, // Position marker to coordinates
            icon:image, //use our image as the marker
            map: map, // assign the market to our map variable
            title: 'Click here for more details' // Marker ALT Text
        });

        google.maps.event.addDomListener(window, 'resize', function() { map.setCenter(myLatlng); }); // Keeps the Pin Central when resizing the browser on responsive sites
    }
    google.maps.event.addDomListener(window, 'load', initialise); // Execute our 'initialise' function once the page has loaded.
</script>
<?php
	}
}