<?php
global $item;

$related_img_url = get_the_post_thumbnail_url($item->ID,'big-square');

?>
	<div class="item" onClick="document.location.href='<?php echo get_permalink($item); ?>';" style="cursor:pointer;">
		<div class="box box-content-overlay">
			<div class="box-content">
				<h3><a href="<?php echo get_permalink($item); ?>"><?php echo $item->post_title; ?></a></h3>
            </div><!-- /box-content -->
			<img src="<?php echo $related_img_url; ?>" alt="<?php echo esc_attr($item->post_title); ?>" title="<?php echo esc_attr($item->post_title); ?>">
		</div><!-- /box-content-overlay -->
	</div><!-- /item -->
<?php

