<?php
global $item;

$related_img_url = get_the_post_thumbnail_url($item->ID,'palco-large');

?>
    <div class="box box-simple">
        <div class="box-thumb">
            <img src="<?php echo $related_img_url; ?>" alt="<?php echo esc_attr($item->post_title); ?>" title="<?php echo esc_attr($item->post_title); ?>">
        </div><!-- /box-thumb -->
        <div class="box-content">
            <h3 class="mb-1"><a href="<?php echo get_permalink($item); ?>"><?php echo $item->post_title; ?></a></h3>
            <span class="cat">
                <?php wayglo_cat(); ?>
            </span><!-- /cat -->
        </div><!-- /box-content -->
    </div>

<?php

