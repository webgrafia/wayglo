<?php
global $post;
$hero_img_url = get_the_post_thumbnail_url($post->ID,'apertura');
?>
<section class="section section-hero mb-3">
	<div class="hero-simple">
		<div class="hero-simple-bg" style="background-image: url('<?php echo $hero_img_url; ?>');"></div>
		<div class="hero-simple-content">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 offset-lg-6">
						<?php wayglo_cat("btn btn-white btn-sm text-uppercase"); ?>
						<h1><?php the_title(); ?></h1>
						<?php the_excerpt(); ?>
						<a class="btn btn-primary btn-lg btn-icon" href="<?php echo get_permalink(); ?>">
							<span>Leggi tutto</span>
							<svg class="svg-arrow-right"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-arrow-right"></use></svg>
						</a>
					</div><!-- /col-lg-6 -->
				</div><!-- /row -->
			</div><!-- /container -->
		</div><!-- /hero-simple-content -->
	</div><!-- /hero-simple -->
</section><!-- /section -->
<?php
