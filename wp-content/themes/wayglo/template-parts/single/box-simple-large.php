<?php
$img_url = get_the_post_thumbnail_url($post->ID,'palco-small');
?>
    <div class="box box-simple-large box-date mb-3">
        <div class="box-thumb">
            <a href="<?php the_permalink(); ?>"><img src="<?php echo $img_url; ?>" alt="<?php echo esc_attr($post->post_title); ?>" title="<?php echo esc_attr($post->post_title); ?>"></a>
        </div><!-- /box-thumb -->
        <div class="box-content">
	        <?php wayglo_cat("btn btn-white btn-sm text-uppercase mb-1"); ?>
            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
	        <?php the_excerpt(); ?>
        </div><!-- /box-content -->
    </div><!-- /box-simple -->
<?php
