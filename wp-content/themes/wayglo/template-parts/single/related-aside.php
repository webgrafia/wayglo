<?php
global $item;
$cat = get_category(get_field("categoria"));

// recupero per categoria
$args = array(
	'posts_per_page'=>6,
	'category__in'   => wp_get_post_categories(get_the_ID()),
	'post_type' => get_post_type(),
	'post__not_in' => array(get_the_ID())
);
$related_post = get_posts($args);

if($related_post){

	?>
<div class="carousel-aside-wrapper mb-2">
	<h3 class="carousel-aside-title text-uppercase">
        <?php echo $cat->name; ?>
        <?php
        /*
        if(get_field("apertura_video", get_the_ID()))
            echo "guarda anche";
        elseif($cat->slug == "le-meraviglie")
	        echo "guarda anche";
        else
            echo "leggi anche";
        */ ?></h3>
	<div class="owl-carousel owl-theme carousel-aside">
	<?php
	foreach ( $related_post as $item ) {
		get_template_part("template-parts/single/card", "custom");
	}
	?>
	</div><!-- /carousel-aside -->
</div>
		<?php
}
