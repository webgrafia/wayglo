<?php
global $item;

$img_url = get_the_post_thumbnail_url($item->ID,'card-small');
?>
	<div class="box box-bg-gradient-horizontal mb-1" style="background-image: url('<?php echo $img_url; ?>');">
		<div class="box-content">
			<span class="cat">
				<a href="<?php echo get_post_type_archive_link("almanacco"); ?>">Accadde oggi</a>
			</span><!-- /cat -->
			<h3><a href="<?php echo get_permalink($item); ?>"><?php echo $item->post_title; ?></a></h3>
			<div class="box-label-icon">
				<span class="small-icon">
					<svg class="svg-calendar"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-calendar"></use></svg>
					<span class="small-icon-text"><?php
						$data_almanacco = get_field("data_almanacco", $item);
						$data_almanacco_testo = get_field("data_almanacco_testo", $item);
						if($data_almanacco_testo){
							echo $data_almanacco_testo;
                        }else if($data_almanacco){
							echo $data_almanacco;
                        }/*else {
						    echo get_the_date();
						}*/
                         ?></span>
				</span>
			</div><!-- /cat -->
		</div><!-- /box-content -->
	</div><!-- /box-content-overlay --><?php
