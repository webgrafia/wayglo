<?php
global $section;
?>
<section class="section section-banners">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="box-banners">
					<div class="box-banners-col d-none d-lg-block ">
						<?php if($section["immagini"][0]["link"]){ ?><a href="<?php echo $section["immagini"][0]["link"]; ?>"><?php } ?>
						<div class="box-banner-half box-banner-bg" style="background-image: url('<?php echo $section["immagini"][0]["immagine"]["url"]; ?>');">
							<?php if($section["immagini"][0]["titolo"]){ ?>
							<div class="box-content">
								<h3><?php echo $section["immagini"][0]["titolo"]; ?></h3>
							</div><!-- /box-content -->
							<?php } ?>
						</div><!-- /box-banner-half -->
						<?php if($section["immagini"][0]["link"]){ ?></a><?php } ?>

						<?php if($section["immagini"][1]["link"]){ ?><a href="<?php echo $section["immagini"][1]["link"]; ?>"><?php } ?>
						<div class="box-banner-half box-banner-bg" style="background-image: url('<?php echo $section["immagini"][1]["immagine"]["url"]; ?>');">
							<?php if($section["immagini"][1]["titolo"]){ ?>
								<div class="box-content">
									<h3><?php echo $section["immagini"][1]["titolo"]; ?></h3>
								</div><!-- /box-content -->
							<?php } ?>
						</div>
						<?php if($section["immagini"][1]["link"]){ ?></a><?php } ?>
					</div><!-- /box-banners-col -->
					<div class="box-banners-col">
						<?php if($section["immagini"][2]["link"]){ ?><a href="<?php echo $section["immagini"][2]["link"]; ?>"><?php } ?>
						<div class="box-banner box-banner-bg" style="background-image: url('<?php echo $section["immagini"][2]["immagine"]["url"]; ?>');">
							<?php if($section["immagini"][2]["titolo"]){ ?>
								<div class="box-content">
									<h3><?php echo $section["immagini"][2]["titolo"]; ?></h3>
								</div><!-- /box-content -->
							<?php } ?>
						</div><!-- /box-banner -->
						<?php if($section["immagini"][2]["link"]){ ?></a><?php } ?>
					</div><!-- /box-banners-col -->
                    <div class="box-banners-col d-lg-none ">
						<?php if($section["immagini"][0]["link"]){ ?><a href="<?php echo $section["immagini"][0]["link"]; ?>"><?php } ?>
                            <div class="box-banner-half box-banner-bg" style="background-image: url('<?php echo $section["immagini"][0]["immagine"]["url"]; ?>');">
								<?php if($section["immagini"][0]["titolo"]){ ?>
                                    <div class="box-content">
                                        <h3><?php echo $section["immagini"][0]["titolo"]; ?></h3>
                                    </div><!-- /box-content -->
								<?php } ?>
                            </div><!-- /box-banner-half -->
							<?php if($section["immagini"][0]["link"]){ ?></a><?php } ?>

						<?php if($section["immagini"][1]["link"]){ ?><a href="<?php echo $section["immagini"][1]["link"]; ?>"><?php } ?>
                            <div class="box-banner-half box-banner-bg" style="background-image: url('<?php echo $section["immagini"][1]["immagine"]["url"]; ?>');">
								<?php if($section["immagini"][1]["titolo"]){ ?>
                                    <div class="box-content">
                                        <h3><?php echo $section["immagini"][1]["titolo"]; ?></h3>
                                    </div><!-- /box-content -->
								<?php } ?>
                            </div>
							<?php if($section["immagini"][1]["link"]){ ?></a><?php } ?>
                    </div><!-- /box-banners-col -->
					<div class="box-banners-col">
						<?php if($section["immagini"][3]["link"]){ ?><a href="<?php echo $section["immagini"][3]["link"]; ?>"><?php } ?>
							<div class="box-banner-half box-banner-bg" style="background-image: url('<?php echo $section["immagini"][3]["immagine"]["url"]; ?>');">
								<?php if($section["immagini"][3]["titolo"]){ ?>
									<div class="box-content">
										<h3><?php echo $section["immagini"][3]["titolo"]; ?></h3>
									</div><!-- /box-content -->
								<?php } ?>
							</div><!-- /box-banner-half -->
							<?php if($section["immagini"][3]["link"]){ ?></a><?php } ?>

						<?php if($section["immagini"][4]["link"]){ ?><a href="<?php echo $section["immagini"][4]["link"]; ?>"><?php } ?>
							<div class="box-banner-half box-banner-bg" style="background-image: url('<?php echo $section["immagini"][4]["immagine"]["url"]; ?>');">
								<?php if($section["immagini"][4]["titolo"]){ ?>
									<div class="box-content">
										<h3><?php echo $section["immagini"][4]["titolo"]; ?></h3>
									</div><!-- /box-content -->
								<?php } ?>
							</div>
							<?php if($section["immagini"][4]["link"]){ ?></a><?php } ?>
					</div><!-- /box-banners-col -->
				</div><!-- /box-banners -->
			</div><!-- /col -->
		</div><!-- /row -->
	</div><!-- /container -->
</section><!-- /section -->
