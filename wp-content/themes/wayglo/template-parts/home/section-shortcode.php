<?php
global $section;
?>
<section class="section section-stage mb-1">
	<div class="container">
		<div class="row">
			<div class="col-12">
                <?php
                echo do_shortcode($section["shortcode"]);
                ?>
			</div><!-- /col -->
		</div><!-- /row -->
	</div><!-- /container -->
</section>