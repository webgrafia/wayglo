<?php
global $section;

$color = $section["colore_riga"];
?>
<section class="section section-row">
	<div class="stage-body">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<hr style="border-top: solid 8px <?php echo $color; ?>;">
				</div>
			</div>
		</div>
	</div>
</section>
