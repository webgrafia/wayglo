<?php
global $section;
?>
<section class="section section-hero mb-1 mb-lg-3">
	<div class="owl-carousel owl-theme carousel-hero-single" data-slider-id="1">
		<?php
		foreach ( $section["slides"] as $slide ) {
			?>
			<div class="item">
				<div class="hero-img" style="background-image: url('<?php echo $slide["immagine"]["sizes"]["apertura"]; ?>');">
					<div class="container">
						<div class="row">
							<div class="col-xl-12">
								<h1 style="white-space: nowrap;"><?php
                                    if($slide["titolo_green"])
                                        echo '<span class="accent">';
                                    echo $slide["titolo"];
									if($slide["titolo_green"])
										echo '</span>';
                                    ?>
                                    <br>
									<?php
									if($slide["titolo_2_green"])
										echo '<span class="accent">';
                                    echo $slide["titolo_2"];
									if($slide["titolo_2_green"])
										echo '</span>';

									?>
                                    <br>
									<?php
									if($slide["titolo_3_green"])
										echo '<span class="accent">';
									echo $slide["titolo_3"];
									if($slide["titolo_3_green"])
										echo '</span>';
									?>
                                </h1>
								<a class="btn btn-primary btn-lg btn-icon" href="<?php echo $slide["link"]; ?>">
									<span><?php echo $slide["cta"]; ?></span>
									<svg class="svg-arrow-right"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-arrow-right"></use></svg>
								</a>
							</div><!-- /col-xl-6 -->
						</div><!-- /row -->
					</div><!-- /container -->
				</div>
			</div><!-- /item -->
		<?php
		}
		?>
	</div><!-- /carousel-hero-single -->
	<div class="owl-thumbs" data-slider-id="1">
		<?php
		foreach ( $section["slides"] as $slide ) {
		?>
			<button class="owl-thumb-item">
				<?php echo wp_get_attachment_image( $slide["immagine"]["ID"], "slider-small" );  ?>
			</button>
			<?php
		}
		?>
	</div>
</section><!-- /section -->
