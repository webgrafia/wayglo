<?php
global $section;
?>
<section class="section section-stage mb-1">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="stage-title stage-title-simple">
					<h2><?php echo $section["titolo"]; ?></h2>
					<?php  if($section["link"]){ ?><a class="btn btn-primary" href="<?php echo $section["link"]; ?>"><?php }else{ ?><span class="btn btn-primary" style="cursor: auto;"><?php } ?>
					<?php echo $section["cta"]; ?>
					<?php  if($section["link"]){ ?></a><?php }else{ ?></span><?php } ?>
				</div><!-- /stage-title -->
			</div><!-- /col -->
		</div><!-- /row -->
	</div><!-- /container -->
</section>