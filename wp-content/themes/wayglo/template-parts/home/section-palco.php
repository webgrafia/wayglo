<?php
global $section;
//echo "<pre>";
//print_r( $section );
//echo "</pre>";

$dimensione = $section["dimensione"]; // piccola, media, grande
if ( $dimensione == "piccola" ) {
	$imgsize = "palco-small";
} else if ( $dimensione == "media" ) {
	$imgsize = "palco-medium";
} else if ( $dimensione == "grande" ) {
	$imgsize = "palco-large";
}

$posizione_testo = $section["posizione_testo"];

?>
<section class="section section-stage mb-1 mb-lg-3">
    <div class="stage-body">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="owl-carousel owl-theme carousel-stage<?php if ( $dimensione == "media" ) {
						echo "-large";
					} ?>">
						<?php
						foreach ( $section["slide"] as $slide ) {
							$mostra_occhiello = $slide["mostra_occhiello"];

							?>
                            <div class="item">
                                <?php if ( $posizione_testo == "sopra" ||  $posizione_testo == "sopra-sotto" )  { ?>
                                    <div class="box box-content-overlay <?php if($slide["font_speciale"]) echo "box-custom " .$slide["allineamento_testo"]; ?>">
                                        <?php display_dot($slide["occhiello"]); ?>
                                        <?php if($mostra_occhiello == "foto") { ?>
                                            <span class="occhiello"  <?php if($slide["colore_occhiello"]) echo 'style="background-color:'.$slide["colore_occhiello"].';"'; ?>>
                                              <a href="<?php echo $slide["link"]; ?>"><?php echo $slide["occhiello"]; ?></a>
                                            </span><!-- /cat -->
                                        <?php } ?>
                                        <div class="box-content">
                                            <?php if($mostra_occhiello == "titolo") { ?>
                                            <span class="cat">
                                              <a href="<?php echo $slide["link"]; ?>"><?php echo $slide["occhiello"]; ?></a>
                                            </span><!-- /cat -->
                                            <?php }  ?>
                                            <?php if(trim($slide["titolo"])) { ?>
                                            <h3><a href="<?php echo $slide["link"]; ?>"><?php echo $slide["titolo"]; ?></a></h3>
                                            <?php } ?>
                                        </div><!-- /box-content -->
	                                    <?php if(!trim($slide["titolo"])) echo '<a href="'.$slide["link"].'">'; ?>
										<?php echo wp_get_attachment_image( $slide["immagine"]["ID"], $imgsize ); ?>
	                                    <?php if(!trim($slide["titolo"])) echo '</a>'; ?>
                                    </div><!-- /box-content-overlay -->
									<?php if ($posizione_testo == "sopra-sotto" )  { ?>
                                    <div class="box box-simple">
                                        <div class="box-content">
                                            <h3><a href="<?php echo $slide["link"]; ?>"><?php echo $slide["sottotitolo"]; ?></a></h3>
                                        </div>
                                    </div>
									<?php } ?>
								<?php } else if ( $posizione_testo == "sotto" ) { ?>
                                    <div class="box box-simple">
	                                    <?php display_dot($slide["occhiello"]); ?>
                                        <div class="box-thumb">
	                                        <?php if(!trim($slide["titolo"])) echo '<a href="'.$slide["link"].'">'; ?>
	                                        <?php echo wp_get_attachment_image( $slide["immagine"]["ID"], $imgsize ); ?>
	                                        <?php if(!trim($slide["titolo"])) echo '</a>'; ?>

                                            <?php if($mostra_occhiello == "foto") { ?>
                                                <span class="occhiello"  <?php if($slide["colore_occhiello"]) echo 'style="background-color:'.$slide["colore_occhiello"].';"'; ?>>
                                              <a href="<?php echo $slide["link"]; ?>"><?php echo $slide["occhiello"]; ?></a>
                                            </span><!-- /cat -->
	                                        <?php } ?>
                                        </div><!-- /box-thumb -->

                                        <div class="box-content">
	                                        <?php if($mostra_occhiello == "titolo") { ?>
                                            <span class="cat">
                                              <a href="<?php echo $slide["link"]; ?>"><?php echo $slide["occhiello"]; ?></a>
                                            </span><!-- /cat -->
	                                        <?php } ?>
                                            <h3><a href="<?php echo $slide["link"]; ?>"><?php echo $slide["titolo"]; ?></a></h3>
                                        </div><!-- /box-content -->
                                    </div><!-- /box-simple -->
                                <?php }else{ ?>
                                    <div class="box">
	                                    <?php display_dot($slide["occhiello"]); ?>
                                        <a href="<?php echo $slide["link"]; ?>">
	                                        <img src="<?php echo wp_get_attachment_image_url( $slide["immagine"]["ID"], $imgsize ); ?>" />
                                        </a>
                                    </div><!-- /box -->
								<?php } ?>
                            </div><!-- /item -->
							<?php
						}
						?>
                    </div><!-- /carousel-stage -->
                </div><!-- /col -->
            </div><!-- /row -->
        </div><!-- /container -->
    </div><!-- /stage-body -->

</section><!-- /section -->
