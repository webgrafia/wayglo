<?php
global $section;
?>
<section class="section section-stage">
	<div class="stage-title-wrapper mb-1 mb-lg-3 mt-3">
		<div class="stage-title-img <?php if($section["sfuma"]) echo "sfuma "; ?>" style="background-image: url('<?php echo $section["immagine"]["sizes"]["thin"]; ?>'); <?php if ( $section["link"] ) echo 'cursor:pointer;'; ?>" <?php if ( $section["link"] ) echo 'onClick="document.location.href=\''.$section["link"].'\'"'; ?>>
			<svg width="100%" height="100%" viewBox="0 0 75 150" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;"><path style="fill: #0c161c;" d="M75,0l0,150l-75,0l75,-150Z"/></svg>
		</div><!-- /stage-title-img -->
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-6 offset-md-6">
					<div class="stage-title">
						<h2 <?php if ( $section["link"] ) echo 'style="cursor:pointer;" onClick="document.location.href=\''.$section["link"].'\'"'; ?>><?php echo $section["titolo"];
                        if($section["titolo_2"]) echo "<br>".$section["titolo_2"];
                        ?></h2>
						<p><?php echo $section["testo"]; ?> &nbsp; <a class="btn btn-primary btn-sm d-lg-none" href="<?php echo $section["link"]; ?>"><?php echo $section["cta"]; ?></a></p>
						<?php  if($section["link"]){ ?>
							<a class="btn btn-primary d-none d-lg-inline" href="<?php echo $section["link"]; ?>"><?php echo $section["cta"]; ?></a>
						<?php } ?>

					</div><!-- /stage-title -->
				</div><!-- /col -->
			</div><!-- /row -->
		</div><!-- /container -->
	</div><!-- /stage-title-wrapper -->
</section>
