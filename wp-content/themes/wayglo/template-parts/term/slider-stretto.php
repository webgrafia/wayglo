<?php
global $posts;


?>
    <section class="section section-hero border-bottom-1 mb-3">
        <div class="owl-carousel owl-theme carousel-hero-normal">
            <?php
            foreach ( $posts as $post ) {
                setup_postdata($post);
	            $hero_img_url = get_the_post_thumbnail_url($post->ID,'slider-small');

	            ?>
                <div class="item">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-5 order-lg-1">
                                <div class="hero-img-normal" style="background-image: url('<?php echo $hero_img_url; ?>');"></div>
                            </div><!-- /col-lg-5 -->
                            <div class="col-lg-5 offset-lg-1 order-lg-0">
	                            <?php wayglo_cat( "btn btn-white btn-sm text-uppercase" , $post); ?>
                                <h1><?php the_title(); ?></h1>
	                            <?php the_excerpt(); ?>
                                <a class="btn btn-primary btn-lg btn-icon" href="<?php echo get_permalink(); ?>">
                                    <span>Leggi tutto</span>
                                    <svg class="svg-arrow-right"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-arrow-right"></use></svg>
                                </a>
                            </div><!-- /col-lg-5 -->
                        </div><!-- /row -->
                    </div><!-- /container -->
                </div><!-- /item -->
                    <?php
            }
            ?>
        </div><!-- /carousel-hero-normal -->
    </section><!-- /section -->

<?php

