<?php
global $i, $term, $block;


$titolo_colonna_1 = get_field('blocchi_'.$i.'_'.'titolo_colonna_1', $term);
$colonna_1 = get_field('blocchi_'.$i.'_'.'colonna_1', $term);
$titolo_colonna_2 = get_field('blocchi_'.$i.'_'.'titolo_colonna_2', $term);
$colonna_2 = get_field('blocchi_'.$i.'_'.'colonna_2', $term);
$titolo_colonna_3 = get_field('blocchi_'.$i.'_'.'titolo_colonna_3', $term);
$colonna_3 = get_field('blocchi_'.$i.'_'.'colonna_3', $term);


?>
<section class="section section-stage mb-3">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <span class="btn btn-primary" ><?php echo $titolo_colonna_1; ?></span>
                <div class="box"><?php echo $colonna_1; ?></div>
            </div>
            <div class="col-sm-4">
                <span class="btn btn-primary" ><?php echo $titolo_colonna_2; ?></span>
                <div class="box"><?php echo $colonna_2; ?></div>
            </div>
            <div class="col-sm-4">
                <span class="btn btn-primary" ><?php echo $titolo_colonna_3; ?></span>
                <div class="box"><?php echo $colonna_3; ?></div>
            </div>
        </div>
    </div>
</section>
