<?php
global $i, $term, $block, $item;


$cat_id =  get_field('blocchi_'.$i.'_'.'categoria', $term);
$tipologia_card =  get_field('blocchi_'.$i.'_'.'tipologia_card', $term);

$cat = get_category($cat_id);

if($cat){

	// recupero articoli di queasta categoria
	$posts = get_posts(array( 'category' => $cat_id));
	if(is_array($posts) && count($posts)){
?>

        <section class="section mb-3">
            <div class="stage-body">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="stage-title-wrapper mb-2">
                                <a class="badge badge-primary" href="<?php echo get_category_link($cat); ?>"><?php echo $cat->name ?></a>
                            </div><!-- /stage-title-wrapper -->
                        </div><!-- /col-12 -->
                    </div><!-- /row -->
                    <div class="grid content-grid">
	                    <?php
	                    foreach ( $posts as $item ) {
	                        echo '<div class="grid-item mb-2">';
		                    get_template_part("template-parts/single/card", $tipologia_card);
		                    echo '</div>';
	                    }
	                    ?>
                    </div><!-- /content-grid -->
                    <div class="grid-pagination pt-2">
                        <a href="<?php echo get_category_link($cat); ?>" class="grid-next btn btn-primary">Mostra altre</a>
                    </div>
                </div><!-- /container -->
            </div><!-- /stage-body -->
        </section><!-- /section -->

<?php
	}
}
	?>