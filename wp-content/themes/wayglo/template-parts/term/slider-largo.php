<?php
global $posts;


?>

    <section class="section section-hero mb-3">
        <div class="owl-carousel owl-theme carousel-hero-simple">
<?php
foreach ( $posts as $post ) {
	setup_postdata($post);
	$hero_img_url = get_the_post_thumbnail_url($post->ID,'slider-small');

	?>
            <div class="item">
                <div class="box box-large">
                    <div class="box-large-content">
                        <div class="box-tags">
	                        <?php wayglo_cat( "badge badge-white" , $post); ?>
                        </div><!-- /box-tags -->
                        <h2><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h2>
                    </div><!-- /box-large-content -->
	                <?php
	                echo get_the_post_thumbnail($post, "slider-small" );
	                ?>

                </div><!-- /box-large -->
            </div><!-- /item -->
	<?php
}
?>
        </div><!-- /carousel-hero-single -->
    </section><!-- /section -->

<?php

