<?php
global $i, $term, $block;

$occhiello = get_field('blocchi_'.$i.'_'.'occhiello', $term);
$titolo = get_field('blocchi_'.$i.'_'.'titolo', $term);
$testo = get_field('blocchi_'.$i.'_'.'testo', $term);
$label = get_field('blocchi_'.$i.'_'.'label', $term);
$link = get_field('blocchi_'.$i.'_'.'link', $term);
$immagine = get_field('blocchi_'.$i.'_'.'immagine', $term);
$url_immagine = $immagine["sizes"]["palco-large"];

?>
	<section class="section section-book mb-3">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 offset-lg-1">
					<div class="section-book-thumb">
						<a href="<?php echo $link; ?>">
							<img src="<?php echo $url_immagine; ?>">
						</a>
					</div><!-- /section-book-thumb -->
				</div><!-- /col-lg-4 -->
				<div class="col-lg-5 offset-lg-1 section-book-content-wrapper">
					<div class="section-book-content">
                        <?php
                        if($occhiello){
                        ?>
						<a href="<?php echo $link; ?>" class="btn btn-white btn-sm text-uppercase mb-1"><?php echo $occhiello; ?></a>
                        <?php
                        }
                        ?>
						<h3><a href="<?php echo $link; ?>"><?php echo $titolo; ?></a></h3>
						<?php echo $testo; ?>
						<a class="btn btn-primary btn-lg btn-icon" href="<?php echo $link; ?>">
							<span><?php echo $label ?></span>
							<svg class="svg-arrow-right"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-arrow-right"></use></svg>
						</a>
					</div><!-- /section-book-content -->
				</div><!-- /col-lg-5 -->
			</div><!-- /row -->
		</div><!-- /container -->
	</section>
<?php
