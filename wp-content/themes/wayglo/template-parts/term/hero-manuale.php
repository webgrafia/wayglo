<?php
global $term;

$titolo = get_field('titolo_apertura', $term);
$testo = get_field('testo_apertura', $term);
$immagine_apertura = get_field('immagine_apertura', $term);
$hero_img_url = $immagine_apertura["sizes"]["apertura"];
?>
<section class="section section-hero mb-3">
	<div class="hero-simple">
		<div class="hero-simple-bg" style="background-image: url('<?php echo $hero_img_url; ?>');"></div>
		<div class="hero-simple-content">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 offset-lg-6">
						<h1><?php echo $titolo; ?></h1>
						<?php echo $testo; ?>
					</div><!-- /col-lg-6 -->
				</div><!-- /row -->
			</div><!-- /container -->
		</div><!-- /hero-simple-content -->
	</div><!-- /hero-simple -->
</section><!-- /section -->
<?php
