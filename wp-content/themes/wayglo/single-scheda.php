<?php
/**
 * The main template file
 */

get_header();

if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();

		$featured_img_url = get_field("immagine_di_copertina");
		if(!$featured_img_url)
    		$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'apertura');

		?>
        <section class="section section-hero section-bg" <?php if($featured_img_url){ ?> style="background-image: url('<?php echo $featured_img_url; ?>');" <?php } ?>>
            <div class="container">
                <div class="row">
                    <div class="col-lg-5 offset-lg-1">
                        <div class="section-hero__content">
	                        <?php wayglo_cat( "btn btn-white btn-sm text-uppercase" ); ?>
                            <h1><?php the_title(); ?></h1>
                            <?php
                            if(get_field("abilita_cta")){
                            ?>
                            <a class="btn btn-primary btn-lg btn-icon scroll-anchor" href="<?php if(get_field("link_esterno_cta")) echo get_field("link_cta"); else echo "#map-wrapper";  ?>">
                                <span><?php echo get_field("label_cta"); ?></span>
                                <svg class="svg-caret-down"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#<?php if(get_field("link_esterno_cta")) echo "svg-arrow-right"; else echo "svg-caret-down"; ?>"></use></svg>
                            </a>
                            <?php
                            }
                            ?>
                        </div><!-- /section-hero__content -->
                    </div><!-- /col-lg-5 -->
                </div><!-- /row -->
            </div><!-- /container -->
        </section><!-- /section -->

        <main role="main" class="article-wrapper bg-white">
            <div class="container">

                <div class="row sticky-sidebar-container">
                    <div class="col-lg-6 offset-lg-1">
                        <article class="article-container">
                            <?php the_content(); ?>
                            <div class="article-footer">
		                        <?php get_template_part("template-parts/common/share-button", "article"); ?>
                            </div><!-- /article-footer -->

	                        <?php echo do_shortcode(get_field("shortcode_after_content", "option")); ?>

                            <?php get_template_part("template-parts/single/licenza"); ?>

                        </article>
                    </div><!-- /col-lg-6 -->
                    <div class="col-lg-4 offset-lg-1">

                        <aside role="complementary" class="article-aside sticky-sidebar">
                            <?php
                            $box_informativi = get_field("box_informativo");
                            if($box_informativi){
                                foreach ($box_informativi as $box){
                                    ?>
                                    <div class="card card-border mb-2">
                                        <div class="card-body">
                                            <h3 class="title-bg title-icon">
                                                <?php  if($box["icona_box"]){ ?>
                                                <svg class="<?php echo $box["icona_box"]; ?>"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#<?php echo $box["icona_box"]; ?>"></use></svg>
                                                <?php }  ?>
                                                <span><?php echo $box["titolo_box"]; ?></span>
                                            </h3>
                                            <ul class="list-description">
	                                            <?php foreach ($box["attributi"] as $attributo){ ?>
                                                <li>
                                                    <span><?php echo $attributo["nome"]; ?></span>
                                                    <p><?php echo $attributo["valore"]; ?></p>
                                                </li>
                                                <?php } ?>
                                            </ul>
                                        </div><!-- /card-body -->
                                    </div><!-- /card-border -->
                                    <?php
                                }
                            }

                            get_template_part("template-parts/single/related", "aside");

                            ?>
                        </aside>
                    </div><!-- /col-lg-4 -->
                </div><!-- /row -->

	            <?php
	            get_template_part("template-parts/single/map",  get_post_type());
	            ?>

            </div><!-- /container -->
        </main>
		<?php
	}
}
?>
<?php
get_template_part("template-parts/single/related",  "scheda");
?>

<?php
get_footer();
