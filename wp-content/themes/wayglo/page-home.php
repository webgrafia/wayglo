<?php
/**
 * Template Name: Homepage
 *
 */
get_header();

$sections = get_field( "sections_home" );
foreach ( $sections as $section ) {
	get_template_part( "template-parts/home/section", $section["acf_fc_layout"] );
}

get_footer();
