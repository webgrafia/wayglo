<?php
/**
 * The main template file
 */

get_header();

if ( have_posts() ) {

	$term = get_queried_object();

	$tipologia_apertura = get_field('tipologia_apertura', $term);
	$apertura_manuale = get_field('apertura_manuale', $term);
	$numero_slide = get_field('numero_slide', $term);

	$template = get_field('template', $term);
	$blocchi = get_field('blocchi', $term);

	$immagine_testata = get_field('immagine_testata', $term);
	$link_testata = get_field('link_testata', $term);
	if($immagine_testata)
	{ ?>

        <section class="section mb-3">
            <div class="container">
                <div class="row sticky-sidebar-container">
                    <div class="col-12">
						<?php if($link_testata) echo "<a href='".$link_testata."'>" ?>
                        <img style="width: 100%; max-width: 100%;" src="<?php echo $immagine_testata["url"]; ?>">
						<?php if($link_testata) echo "</a>" ?>
                    </div>
                </div>
            </div>
        </section>
		<?php
	}

    $skip = 0;
	$origposts = $posts;
	switch($tipologia_apertura){

		case "slider_stretto":
			unset($posts);
			$o = 0;
			for($i=0;$i<$numero_slide;$i++){
				if($apertura_manuale && $apertura_manuale[$i]){
					$posts[] =  get_post($apertura_manuale[$i]);
				}else{
					$posts[] = $origposts[$o];
					$o++;
				}
			}
			$excludeposts = $posts;
		//	$skip = $o;
			get_template_part("template-parts/term/slider", "stretto");
			break;

		case "slider_largo":
			unset($posts);
			$o = 0;
			for($i=0;$i<$numero_slide;$i++){
				if($apertura_manuale && $apertura_manuale[$i]){
					$posts[] =  get_post($apertura_manuale[$i]);
				}else{
					$posts[] = $origposts[$o];
					$o++;
				}
			}
			$excludeposts = $posts;
		//	$skip = $o;
			get_template_part("template-parts/term/slider", "largo");
			break;

		case "singola":
			$post = $posts[0];
			//$skip = 1;
			if($apertura_manuale && count($apertura_manuale)){
				$post = $apertura_manuale[0];
				$skip = 0;
			}
    		$excludeposts[] = $post;
			get_template_part("template-parts/single/hero");
			break;
		case "manuale":
			get_template_part("template-parts/term/hero-manuale");
			break;

        case "nessuna":
            echo "<div class='mb-2'></div>";
            break;

		case "":
		default:
			$post = $posts[0];
			$skip = 1;
		    $excludeposts[] = array();
            get_template_part("template-parts/single/hero");
			break;
	}
	$posts = $origposts;
	wp_reset_postdata();

	foreach ( $excludeposts as $excludepost ) {
		$arrposts[] = $excludepost->ID;
	}

	switch($template){
		case "configurabile":

			$term = get_queried_object();
			$taxonomy = $term->taxonomy;
			$term_id = $term->term_id;
			$blocks = get_term_meta($term_id, "blocchi", true);
			$i=0;
			foreach ($blocks as $block){
				get_template_part("template-parts/term/section", $block);
				$i++;
			}

            break;

		case "2-colonne":
			?>
            <section class="section mb-3">
                <div class="stage-body">
                    <div class="container">
                        <div class="row sticky-sidebar-container mb-3">
                            <div class="col-md-8">
                                <div class="row">
									<?php
									$c=1;
									while ( have_posts() ) {
										the_post();
                                        if(in_array($post->ID, $arrposts))
                                            continue;
										if($c > $skip){
											?>
                                            <div class="col-md-6">
												<?php
												get_template_part("template-parts/single/box-simple-large");
												?>
                                            </div><!-- /col-md-6 -->
											<?php
										}
										$c++;
									}
									?>
                                </div><!-- /row -->
                            </div><!-- /col-md-9 -->
                            <div class="col-md-4">
                                <aside role="complementary" class="section-aside sticky-sidebar">
									<?php
									get_template_part("template-parts/banner/aside");
									?>
                                </aside>
                            </div><!-- /col-md-4 -->
                        </div><!-- /row -->
                        <div class="row mb-3">
                            <div class="col-12">
								<?php
								get_template_part("template-parts/banner/archive");
								?>
                            </div><!-- /col-12 -->
                        </div><!-- /row -->
                    </div><!-- /container -->
                </div><!-- /stage-body -->
            </section><!-- /section -->
			<?php
			break;

		case "3-colonne":
			?>
            <section class="section mb-3">
                <div class="container">
                    <div class="row sticky-sidebar-container">
                        <div class="col-lg-9">
                            <div class="row">

								<?php
								$c=1;
								while ( have_posts() ) {
									the_post();
									if(in_array($post->ID, $arrposts))
										continue;
									if($c > $skip){
										?>
                                        <div class="col-md-4">
											<?php
											get_template_part("template-parts/single/box-content-overlay");
											?>
                                        </div><!-- /col-md-6 -->
										<?php
									}
									$c++;
								}
								?>

                            </div><!-- /row -->
							<?php get_template_part("template-parts/common/pager"); ?>
                        </div><!-- /col-lg-9 -->
                        <div class="col-lg-3">
                            <aside role="complementary" class="section-aside sticky-sidebar">
								<?php
								get_template_part("template-parts/banner/aside", $banner);
								?>
                            </aside>
                        </div><!-- /col-lg-3 -->
                    </div><!-- /row -->
                </div><!-- /container -->
            </section><!-- /section -->


            <section class="section section-stage mb-3">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
							<?php
							get_template_part("template-parts/banner/archive", $banner);
							?>
                        </div><!-- /col-12 -->
                    </div><!-- /row -->
                </div><!-- /container -->
            </section><!-- /section -->
			<?php

			break;
		// se non ho selezionato un template, vado su una colonna
		default:
			$c=1;$r=1;$banner=1;
			while ( have_posts() ) {
				the_post();
				if(in_array($post->ID, $arrposts))
					continue;
				if($c > $skip){
					// apro la sezione
					if(($r == 1) || ($r == 5)) {
						?>
                        <section class="section">
                        <div class="container">
                        <div class="row sticky-sidebar-container">
                        <div class="col-lg-8">
						<?php
					}

					get_template_part("template-parts/single/box-article-horizontal");
					// chiudo la sezione
					if(($r == 4) || ($r == (count($posts) - $skip))) { ?>
                        </div><!-- /col-lg-8 -->
                        <div class="col-lg-4">
                            <aside role="complementary" class="section-aside sticky-sidebar">
								<?php
								get_template_part("template-parts/banner/aside", $banner);
								?>
                            </aside>
                        </div><!-- /col-lg-4 -->
                        </div><!-- /row -->
                        </div><!-- /container -->
                        </section><!-- /section -->
						<?php
						get_template_part("template-parts/banner/archive", "default");


					}
					$r ++;
				}
				$c++;
			}
			get_template_part( "template-parts/common/pager" );
			break;
	}
}else{

	get_template_part("template-parts/common/404");

}
?>
<?php
get_footer();
