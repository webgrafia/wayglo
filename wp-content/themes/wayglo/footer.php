
<footer id="footer-wrapper" class="footer-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-lg-2 d-none d-lg-block">
				<a class="logo-wrapper logo-<?php echo strtolower(get_field("brand", "option")); ?>" href="#">
					<svg width="100%" height="100%" viewBox="0 0 130 36" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;"><path id="Fill-1" d="M26.061,17.893c-0.104,-0.449 -0.188,-0.798 -0.25,-1.049c-0.053,0.219 -0.126,0.527 -0.22,0.924c-0.24,1.012 -0.422,1.727 -0.548,2.144l-2.786,9.784l-5.808,0l-7.547,-27.237c-5.324,3.12 -8.902,8.898 -8.902,15.515c0,9.926 8.047,17.974 17.974,17.974c4.516,0 8.639,-1.67 11.796,-4.42l-3.192,-11.616c-0.105,-0.344 -0.277,-1.017 -0.517,-2.019Zm65.729,3.95c0,6.998 -5.673,12.67 -12.67,12.67c-6.998,-0 -12.671,-5.672 -12.671,-12.67c0,-6.998 5.673,-12.67 12.671,-12.67c6.997,-0 12.67,5.672 12.67,12.67Zm17.251,-0.014c0,-3.243 0.903,-5.767 2.71,-7.574c1.806,-1.806 4.322,-2.71 7.547,-2.71c3.306,0 5.853,0.888 7.641,2.663c1.788,1.775 2.683,4.261 2.683,7.458c0,2.322 -0.391,4.225 -1.172,5.711c-0.781,1.486 -1.911,2.642 -3.388,3.469c-1.476,0.826 -3.317,1.24 -5.521,1.24c-2.24,0 -4.094,-0.357 -5.561,-1.071c-1.468,-0.713 -2.658,-1.843 -3.571,-3.387c-0.912,-1.545 -1.368,-3.477 -1.368,-5.799Zm-64.778,6.639l-6.968,0l-0.968,3.279l-6.267,0l7.465,-19.863l6.695,0l7.464,19.863l-6.427,0l-0.994,-3.279Zm48.624,-16.584l6.138,0l-0,14.971l9.579,0l0,4.891l-15.717,0l-0,-19.862Zm-45.836,0l6.817,0l4.002,6.701l4.01,-6.701l6.781,0l-7.723,11.543l-0,8.319l-6.151,0l-0,-8.319l-7.736,-11.543Zm32.26,12.325l-0,-4.476l9.484,-0l-0,8.812c-1.815,1.238 -3.421,2.079 -4.816,2.527c-1.396,0.447 -3.051,0.671 -4.966,0.671c-2.358,-0 -4.279,-0.402 -5.765,-1.206c-1.486,-0.804 -2.638,-2.001 -3.455,-3.591c-0.817,-1.589 -1.226,-3.414 -1.226,-5.474c-0,-2.168 0.447,-4.053 1.341,-5.656c0.894,-1.603 2.204,-2.82 3.929,-3.652c1.346,-0.641 3.157,-0.962 5.434,-0.962c2.194,-0 3.836,0.199 4.924,0.597c1.089,0.397 1.992,1.014 2.71,1.849c0.718,0.836 1.258,1.894 1.619,3.177l-5.92,1.057c-0.244,-0.75 -0.658,-1.323 -1.24,-1.721c-0.583,-0.397 -1.326,-0.596 -2.229,-0.596c-1.346,-0 -2.419,0.467 -3.218,1.403c-0.799,0.934 -1.199,2.413 -1.199,4.437c-0,2.149 0.404,3.685 1.213,4.606c0.808,0.922 1.935,1.382 3.38,1.382c0.686,-0 1.341,-0.099 1.965,-0.298c0.623,-0.198 1.336,-0.537 2.14,-1.016l-0,-1.87l-4.105,-0Zm35.868,-2.353c0,2.005 0.372,3.446 1.118,4.322c0.745,0.876 1.758,1.314 3.041,1.314c1.319,0 2.339,-0.429 3.062,-1.287c0.723,-0.858 1.084,-2.398 1.084,-4.62c0,-1.87 -0.377,-3.236 -1.131,-4.098c-0.754,-0.863 -1.778,-1.295 -3.069,-1.295c-1.238,0 -2.231,0.439 -2.981,1.315c-0.749,0.876 -1.124,2.326 -1.124,4.349Zm-72.189,2.317l-2.193,-7.14l-2.169,7.14l4.362,0Zm-11.085,-3.446c-0.104,-0.562 -0.205,-1.069 -0.302,-1.519c-0.115,-0.532 -0.24,-1.044 -0.376,-1.534l-2.88,-10.552l-5.041,0l-2.88,10.614c-0.219,0.888 -0.384,1.571 -0.493,2.051c-0.11,0.48 -0.212,0.939 -0.305,1.378c-0.105,-0.595 -0.217,-1.174 -0.337,-1.738c-0.12,-0.563 -0.243,-1.127 -0.368,-1.691l-4.422,-17.398c1.124,-0.22 2.285,-0.338 3.473,-0.338c7.914,0 14.628,5.116 17.028,12.22l-3.125,8.352l0.028,0.155c0.019,0.103 0.038,0.207 0.058,0.313l-0.058,-0.313Z" style="fill:#fff;"/></svg>
					<span><?php echo (get_field("brand", "option")); ?></span>
				</a>
			</div><!-- /col-lg-2 -->

			<?php if(wp_get_nav_menu_name("footer1")){ ?>
			<div class="col-lg-2 d-none d-lg-block">
				<h3><?php echo wp_get_nav_menu_name("footer1"); ?></h3>
                <?php
                wp_nav_menu( array(
	                'menu_class'        => "", // (string) CSS class to use for the ul element which forms the menu. Default 'menu'.
	                'container'         => "", // (string) Whether to wrap the ul, and what to wrap it with. Default 'div'.
	                'container_class'   => "", // (string) Class that is applied to the container. Default 'menu-{menu slug}-container'.
	                'container_id'      => "", // (string) The ID that is applied to the container.
	                'before'            => "", // (string) Text before the link markup.
	                'after'             => "", // (string) Text after the link markup.
	                'link_before'       => "", // (string) Text before the link text.
	                'link_after'        => "", // (string) Text after the link text.
	                'theme_location'    => "footer1", // (string) Theme location to be used. Must be registered with register_nav_menu() in order to be selectable by the user.
                ) );
                ?>
			</div><!-- /col-lg-2 -->
            <?php } ?>

			<?php if(wp_get_nav_menu_name("footer2")){ ?>
			<div class="col-lg-2 d-none d-lg-block">
				<h3><?php echo wp_get_nav_menu_name("footer2"); ?></h3>
				<?php
				wp_nav_menu( array(
					'menu_class'        => "", // (string) CSS class to use for the ul element which forms the menu. Default 'menu'.
					'container'         => "", // (string) Whether to wrap the ul, and what to wrap it with. Default 'div'.
					'container_class'   => "", // (string) Class that is applied to the container. Default 'menu-{menu slug}-container'.
					'container_id'      => "", // (string) The ID that is applied to the container.
					'before'            => "", // (string) Text before the link markup.
					'after'             => "", // (string) Text after the link markup.
					'link_before'       => "", // (string) Text before the link text.
					'link_after'        => "", // (string) Text after the link text.
					'theme_location'    => "footer2", // (string) Theme location to be used. Must be registered with register_nav_menu() in order to be selectable by the user.
				) );
				?>
			</div><!-- /col-lg-2 -->
            <?php } ?>

			<?php if(wp_get_nav_menu_name("footer3")){ ?>
			<div class="col-lg-2 d-none d-lg-block">
				<h3><?php echo wp_get_nav_menu_name("footer3"); ?></h3>
				<?php
				wp_nav_menu( array(
					'menu_class'        => "", // (string) CSS class to use for the ul element which forms the menu. Default 'menu'.
					'container'         => "", // (string) Whether to wrap the ul, and what to wrap it with. Default 'div'.
					'container_class'   => "", // (string) Class that is applied to the container. Default 'menu-{menu slug}-container'.
					'container_id'      => "", // (string) The ID that is applied to the container.
					'before'            => "", // (string) Text before the link markup.
					'after'             => "", // (string) Text after the link markup.
					'link_before'       => "", // (string) Text before the link text.
					'link_after'        => "", // (string) Text after the link text.
					'theme_location'    => "footer3", // (string) Theme location to be used. Must be registered with register_nav_menu() in order to be selectable by the user.
				) );
				?>
			</div><!-- /col-lg-2 -->
            <?php } ?>

			<?php if(wp_get_nav_menu_name("footer4")){ ?>

            <div class="col-lg-2 d-none d-lg-block">
				<h3><?php echo wp_get_nav_menu_name("footer4"); ?></h3>
				<?php
				wp_nav_menu( array(
					'menu_class'        => "", // (string) CSS class to use for the ul element which forms the menu. Default 'menu'.
					'container'         => "", // (string) Whether to wrap the ul, and what to wrap it with. Default 'div'.
					'container_class'   => "", // (string) Class that is applied to the container. Default 'menu-{menu slug}-container'.
					'container_id'      => "", // (string) The ID that is applied to the container.
					'before'            => "", // (string) Text before the link markup.
					'after'             => "", // (string) Text after the link markup.
					'link_before'       => "", // (string) Text before the link text.
					'link_after'        => "", // (string) Text after the link text.
					'theme_location'    => "footer4", // (string) Theme location to be used. Must be registered with register_nav_menu() in order to be selectable by the user.
				) );
				?>
			</div><!-- /col-lg-2 -->
			<?php } ?>

            <?php if(wp_get_nav_menu_name("footer5")){ ?>
			<div class="col-lg-2 d-none d-lg-block">
				<h3><?php echo wp_get_nav_menu_name("footer5"); ?></h3>
				<?php
				wp_nav_menu( array(
					'menu_class'        => "", // (string) CSS class to use for the ul element which forms the menu. Default 'menu'.
					'container'         => "", // (string) Whether to wrap the ul, and what to wrap it with. Default 'div'.
					'container_class'   => "", // (string) Class that is applied to the container. Default 'menu-{menu slug}-container'.
					'container_id'      => "", // (string) The ID that is applied to the container.
					'before'            => "", // (string) Text before the link markup.
					'after'             => "", // (string) Text after the link markup.
					'link_before'       => "", // (string) Text before the link text.
					'link_after'        => "", // (string) Text after the link text.
					'theme_location'    => "footer5", // (string) Theme location to be used. Must be registered with register_nav_menu() in order to be selectable by the user.
				) );
				?>
			</div><!-- /col-lg-2 -->
            <?php } ?>
		</div><!-- /row -->
		<div class="row pt-1">
            <div class="col-12 pb-1">
                <div class="footer-links">
		            <?php
		            $footerlinks = wp_nav_menu( array(
			            'menu_class'        => "", // (string) CSS class to use for the ul element which forms the menu. Default 'menu'.
			            'container'         => "", // (string) Whether to wrap the ul, and what to wrap it with. Default 'div'.
			            'container_class'   => "", // (string) Class that is applied to the container. Default 'menu-{menu slug}-container'.
			            'container_id'      => "", // (string) The ID that is applied to the container.
			            'before'            => "", // (string) Text before the link markup.
			            'after'             => "", // (string) Text after the link markup.
			            'link_before'       => "", // (string) Text before the link text.
			            'link_after'        => "", // (string) Text after the link text.
			            'echo'              => false,
			            'theme_location'    => "underfooter", // (string) Theme location to be used. Must be registered with register_nav_menu() in order to be selectable by the user.
		            ) );
		            echo strip_tags($footerlinks, "<a>");
		            ?>
                </div>
            </div>
			<div class="col-lg-3">
                <div class="footer-bottom">
					<?php echo (get_field("area_footer", "option")); ?>
                </div>
			</div><!-- /col-lg-6 -->
            <div class="col-lg-6 footer-disclaimer pt-1">
                <p><?php echo (get_field("testo_footer", "option")); ?></p>
            </div>
            <div class="col-lg-3 footer-details">
                <div class="footer-sponsors">
                        <a href="https://www.typimedia.it/" target "_blank"><img style="max-width: 100%; width: 100px;" src="https://basilicata.wayglo.it/wp-content/themes/wayglo/assets/img/logo-typimedia.png"/></a>
                </div>

				<div class="footer-social">

                    <?php if(get_field("facebook", "option")): ?>
					<a href="<?php echo (get_field("facebook", "option")); ?>">
						<svg class="svg-social-facebook"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-social-facebook"></use></svg>
					</a>
                    <?php endif; ?>
					<?php if(get_field("instagram", "option")): ?>
                    <a href="<?php echo (get_field("instagram", "option")); ?>">
						<svg class="svg-social-instagram"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-social-instagram"></use></svg>
					</a>
					<?php endif; ?>
					<?php if(get_field("twitter", "option")): ?>
                    <a href="<?php echo (get_field("twitter", "option")); ?>">
						<svg class="svg-social-twitter"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-social-twitter"></use></svg>
					</a>
					<?php endif; ?>
					<?php if(get_field("youtube", "option")): ?>
                    <a href="<?php echo (get_field("youtube", "option")); ?>">
						<svg class="svg-social-youtube"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-social-youtube"></use></svg>
					</a>
					<?php endif; ?>
				</div><!-- /header-social -->
			</div><!-- /col-lg-6 -->
		</div><!-- /row -->
	</div><!-- /container -->
</footer>

</div><!-- /push_container -->

<div class="search">
	<button id="btn-search-close" class="btn btn--search-close" aria-label="Close search form"><svg class="svg-search"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-close"></use></svg></button>
	<form class="search__form" action="<?php echo home_url(); ?>">
		<input class="search__input" name="s" type="search" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" value="<?php echo get_search_query() ?>" />
		<span class="search__info">Premi invio per iniziare la ricerca</span>
	</form>
</div><!-- /search -->

<?php wp_footer(); ?>

<script src="<?php echo get_template_directory_uri(); ?>/assets/js/main.js"></script>

</body>
</html>
